Weightlifting Warm-Up Set Plate Calculator
==========================================

1.  I have a 45lb barbell.
2.  When I'm going to lift at any given weight, I like to start with an
    empty bar to warm up.
3.  As I work my way to the target weight, I don't want to add more than
    45lbs at a time (i.e. per warm-up set).
4.  Once I reach the target weight, I'd like to use the lightest plates
    possible.
5.  Of the plates that must go on eventually, I want to add the heaviest
    of them as soon in this process as possible. (For example, if I must
    use 45lb plates, I want them on as soon as my total weight is
    greater than or equal to 135lbs.)
6.  For some exercises (deadlift, bent-over rows), I have 12.5lb pulls
    that I'll want to add ASAP.

I like to listen to things while lifting, and I don't like doing this
arithmetic every time I set out to perform an exercise, so I made myself
a [color-coded chart][1] to essentially figure all this for me. For the
most part, this gets it done!

[1]: https://web.archive.org/web/20240512153150/https://matt.istheguy.com/weights.html

But!

1.  It's not in any way accessible with colors turned off.
2.  If someone other than myself wanted to use it, they would need to
    have the same plates that I have, and the same eyesight.
3.  Sometimes I like to do a cool-down set at 75% of the goal weight,
    and my current solution doesn't factor that in at all.
4.  Nothing for anyone with a different weight barbell let alone a
    different ideal maximum increment.
5.  While pulls are accounted-for, what if I got some alternate
    wagon-wheel pulls? What if I wanted them to be a less-than-ideal
    option for high-weight applications?

The static site is ever so much better than calculating in my head every
time, but that doesn't mean I can't do better.

My intention is to make something with HTML, CSS, JavaScript, and SVG.
It should present as a Progressive Web Application, so that it can look
nice on a phone and so that it can leverage local-storage to store a
configuration of what weights are available and so on.

I could use a framework, but it's been ages since I just built
something, so I'd like to at least try and just build something. Once
built, I'll maybe consider something like webpack.

*MAYBE.*

Actual plate measurements
-------------------------

I'm sure I'll want to use this in some way when calculating how to
display differently-sized weights. At some point, I ought to measure my
old plates (and my way-back plates from when I had a 10lb barbell if
that's at all possible).

Bar has diameter 1.25 for hands, 2 for weights, and 3 (1.25 length)
between the two.

| Weight | Width | Diameter | Metal Band | Volume?  |
| ------ | ----- | -------- | ---------- | -------- |
|   1.25 |   1/2 |   5 5/16 |      0     |   3.0278 |
|   2.5  |   5/8 |   6 3/8  |      0     |   5.7251 |
|   5    |   3/4 |   7 1/2  |      0     |   9.7969 |
|  10    | 1     |   9      |      0     |  19.2500 |
|  15    | 1     |  17 3/4  |        1/4 |  77.7656 |
|  25    | 1 3/8 |  17 3/4  |      2 7/8 | 106.9277 |
|  35    | 1 3/4 |  17 3/4  |      2 7/8 | 136.0898 |
|  45    | 2 1/8 |  17 3/4  |      2 7/8 | 165.2520 |

Pulls add 11.5 to bar height when lying on ground. 3x3 and 2x3x18.25.

I guess let's multiply by 16 and talk in 16ths of an inch!

| Weight | Width | Diameter | Metal Band |
| ------ | ----- | -------- | ---------- |
|   1.25 |     8 |       85 |          0 |
|   2.5  |    10 |      102 |          0 |
|   5    |    12 |      120 |          0 |
|  10    |    16 |      144 |          0 |
|  15    |    16 |      284 |          4 |
|  25    |    22 |      284 |         46 |
|  35    |    28 |      284 |         46 |
|  45    |    34 |      284 |         46 |
|  55    |    40 |      284 |         46 |

Continuing here, handle is 20, weight bar 32, separator 48 (20 wide).
Pulls are 32x48x292 base under a 48x48x152 stand.
