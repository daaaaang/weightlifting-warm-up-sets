// Weightlifting Warm-Up Set Plate Calculator
// Copyright (C) 2024  Matthew Alexander LaChance
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { beforeEach, describe, it } from 'node:test'
import { expect } from 'chai'
import { WeightNumber } from '../lib/weight_number.js'

let subject

describe('WeightNumber(1, 0)', () => {
  it('should raise an exception', () => {
    expect(() => {
      subject = WeightNumber(1, 0)
    }).to.throw()
  })
})

describe('WeightNumber(5, 0)', () => {
  it('should raise an exception', () => {
    expect(() => {
      subject = WeightNumber(5, 0)
    }).to.throw()
  })
})

describe('WeightNumber(0)', () => {
  beforeEach(() => {
    subject = WeightNumber(0)
  })

  it('should have numerator 0', () => {
    expect(subject.numerator).to.equal(0)
  })

  it('should have denominator 1', () => {
    expect(subject.denominator).to.equal(1)
  })

  it('should serialize to { numerator: 0, denominator: 1 }', () => {
    expect(JSON.stringify(subject)).to.equal(JSON.stringify({ numerator: 0, denominator: 1 }))
  })

  it('should print 0', () => {
    expect(`${subject}`).to.equal('0')
  })

  it('should double to 0', () => {
    expect(`${subject.double()}`).to.equal('0')
  })

  it('should half to 0', () => {
    expect(`${subject.half()}`).to.equal('0')
  })

  it('should be less than 1', () => {
    expect(subject.cmp(1)).to.be.below(0)
  })

  it('should be equal to 0', () => {
    expect(subject.cmp(0)).to.equal(0)
  })

  it('should be greater than -1', () => {
    expect(subject.cmp(-1)).to.be.above(0)
  })
})

describe('WeightNumber(1)', () => {
  beforeEach(() => {
    subject = WeightNumber(1)
  })

  it('should have numerator 1', () => {
    expect(subject.numerator).to.equal(1)
  })

  it('should have denominator 1', () => {
    expect(subject.denominator).to.equal(1)
  })

  it('should serialize to { numerator: 1, denominator: 1 }', () => {
    expect(JSON.stringify(subject)).to.equal(JSON.stringify({ numerator: 1, denominator: 1 }))
  })

  it('should print 1', () => {
    expect(`${subject}`).to.equal('1')
  })

  it('should double to 2', () => {
    expect(`${subject.double()}`).to.equal('2')
  })

  it('should half to 1/2', () => {
    expect(`${subject.half()}`).to.equal('\u00bd')
  })
})

describe('WeightNumber(2)', () => {
  beforeEach(() => {
    subject = WeightNumber(2)
  })

  it('should have numerator 2', () => {
    expect(subject.numerator).to.equal(2)
  })

  it('should have denominator 1', () => {
    expect(subject.denominator).to.equal(1)
  })

  it('should serialize to { numerator: 2, denominator: 1 }', () => {
    expect(JSON.stringify(subject)).to.equal(JSON.stringify({ numerator: 2, denominator: 1 }))
  })
})

const ratios = [
  { init: [1, 2], ratio: [1, 2] },
  { init: [2, 4], ratio: [1, 2] },
  { init: [-5, -10], ratio: [1, 2] },
  { init: [5, 15], ratio: [1, 3] },
  { init: [35, 14], ratio: [5, 2] },
  { init: [-1, 2], ratio: [-1, 2] },
  { init: [1, -2], ratio: [-1, 2] },
  { init: [1.25, 1], ratio: [5, 4] },
  { init: [2.5, 1], ratio: [5, 2] },
  { init: [12.5, 1], ratio: [25, 2] }
]

ratios.forEach(o => {
  const { init, ratio } = o

  describe(`WeightNumber(${init[0]}, ${init[1]})`, () => {
    beforeEach(() => {
      subject = WeightNumber(...init)
    })

    it(`should have numerator ${ratio[0]}`, () => {
      expect(subject.numerator).to.equal(ratio[0])
    })

    it(`should have denominator ${ratio[1]}`, () => {
      expect(subject.denominator).to.equal(ratio[1])
    })

    it(`should serialize to { numerator: ${ratio[0]}, denominator: ${ratio[1]} }`, () => {
      expect(JSON.stringify(subject)).to.equal(JSON.stringify({ numerator: ratio[0], denominator: ratio[1] }))
    })
  })
})

describe('WeightNumber(1, 2)', () => {
  beforeEach(() => {
    subject = WeightNumber(1, 2)
  })

  it('should print \u00bd', () => {
    expect(`${subject}`).to.equal('\u00bd')
  })

  it('should double to 1', () => {
    expect(`${subject.double()}`).to.equal('1')
  })

  it('should half to 1/4', () => {
    expect(`${subject.half()}`).to.equal('\u00bc')
  })
})

describe('WeightNumber(WeightNumber(11, 12))', () => {
  beforeEach(() => {
    subject = WeightNumber(WeightNumber(11, 12))
  })

  it('should have numerator 11', () => {
    expect(subject.numerator).to.equal(11)
  })

  it('should have denominator 12', () => {
    expect(subject.denominator).to.equal(12)
  })
})

describe('WeightNumber(1, 2).add(WeightNumber(2, 3))', () => {
  beforeEach(() => {
    subject = WeightNumber(1, 2).add(WeightNumber(2, 3))
  })

  it('should have numerator 7', () => {
    expect(subject.numerator).to.equal(7)
  })

  it('should have denominator 6', () => {
    expect(subject.denominator).to.equal(6)
  })
})

describe('WeightNumber(2, 3).add(WeightNumber(3, 4))', () => {
  beforeEach(() => {
    subject = WeightNumber(2, 3).add(WeightNumber(3, 4))
  })

  it('should have numerator 17', () => {
    expect(subject.numerator).to.equal(17)
  })

  it('should have denominator 12', () => {
    expect(subject.denominator).to.equal(12)
  })
})

describe('WeightNumber(1, 3).add(WeightNumber(1, 6))', () => {
  beforeEach(() => {
    subject = WeightNumber(1, 3).add(WeightNumber(1, 6))
  })

  it('should have numerator 1', () => {
    expect(subject.numerator).to.equal(1)
  })

  it('should have denominator 2', () => {
    expect(subject.denominator).to.equal(2)
  })
})

describe('WeightNumber(1, 2).sub(WeightNumber(2, 3))', () => {
  beforeEach(() => {
    subject = WeightNumber(1, 2).sub(WeightNumber(2, 3))
  })

  it('should have numerator -1', () => {
    expect(subject.numerator).to.equal(-1)
  })

  it('should have denominator 6', () => {
    expect(subject.denominator).to.equal(6)
  })
})

describe('WeightNumber(2, 3).sub(WeightNumber(3, 4))', () => {
  beforeEach(() => {
    subject = WeightNumber(2, 3).sub(WeightNumber(3, 4))
  })

  it('should have numerator -1', () => {
    expect(subject.numerator).to.equal(-1)
  })

  it('should have denominator 12', () => {
    expect(subject.denominator).to.equal(12)
  })
})

describe('WeightNumber(1, 3).sub(WeightNumber(1, 6))', () => {
  beforeEach(() => {
    subject = WeightNumber(1, 3).sub(WeightNumber(1, 6))
  })

  it('should have numerator 1', () => {
    expect(subject.numerator).to.equal(1)
  })

  it('should have denominator 6', () => {
    expect(subject.denominator).to.equal(6)
  })
})

const prints = [
  { num: 1, den: 4, str: '\u00bc' },
  { num: 1, den: 2, str: '\u00bd' },
  { num: 3, den: 4, str: '\u00be' },
  { num: 1, den: 7, str: '\u2150' },
  { num: 1, den: 9, str: '\u2151' },
  { num: 1, den: 10, str: '\u2152' },
  { num: 1, den: 3, str: '\u2153' },
  { num: 2, den: 3, str: '\u2154' },
  { num: 1, den: 5, str: '\u2155' },
  { num: 2, den: 5, str: '\u2156' },
  { num: 3, den: 5, str: '\u2157' },
  { num: 4, den: 5, str: '\u2158' },
  { num: 1, den: 6, str: '\u2159' },
  { num: 5, den: 6, str: '\u215a' },
  { num: 1, den: 8, str: '\u215b' },
  { num: 3, den: 8, str: '\u215c' },
  { num: 5, den: 8, str: '\u215d' },
  { num: 7, den: 8, str: '\u215e' },
  { num: 5, den: 4, str: '1\u2064\u00bc' },
  { num: 10, den: 4, str: '2\u2064\u00bd' },
  { num: 15, den: 4, str: '3\u2064\u00be' },
  { num: 25, den: 2, str: '12\u2064\u00bd' }
]

prints.forEach(o => {
  describe(`WeightNumber(${o.num}, ${o.den})`, () => {
    beforeEach(() => {
      subject = WeightNumber(o.num, o.den)
    })

    it(`should print ${o.str}`, () => {
      expect(`${subject}`).to.equal(o.str)
    })
  })
})

describe('WeightNumber("2", "3")', () => {
  beforeEach(() => {
    subject = WeightNumber('2', '3')
  })

  it('should print \u2154', () => {
    expect(`${subject}`).to.equal('\u2154')
  })

  it('should be greater than 1/2', () => {
    expect(subject.cmp(0.5)).to.be.above(0)
  })

  it('should be greater than 0.666', () => {
    expect(subject.cmp(0.666)).to.be.above(0)
  })

  it('should be equal to 2/3', () => {
    expect(subject.cmp(WeightNumber(2, 3))).to.equal(0)
  })

  it('should be less than 1', () => {
    expect(subject.cmp(1)).to.be.below(0)
  })
})

describe('WeightNumber("2/3")', () => {
  beforeEach(() => {
    subject = WeightNumber('2/3')
  })

  it('should print \u2154', () => {
    expect(`${subject}`).to.equal('\u2154')
  })

  it('should be greater than 1/2', () => {
    expect(subject.cmp(0.5)).to.be.above(0)
  })

  it('should be greater than 0.666', () => {
    expect(subject.cmp(0.666)).to.be.above(0)
  })

  it('should be equal to 2/3', () => {
    expect(subject.cmp(WeightNumber(2, 3))).to.equal(0)
  })

  it('should be less than 1', () => {
    expect(subject.cmp(1)).to.be.below(0)
  })
})
