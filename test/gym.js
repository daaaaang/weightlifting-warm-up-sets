// Weightlifting Warm-Up Set Plate Calculator
// Copyright (C) 2024  Matthew Alexander LaChance
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { beforeEach, describe, it } from 'node:test'
import { expect } from 'chai'
import { Gym } from '../lib/gym.js'
import { WeightNumber } from '../lib/weight_number.js'

let subject

const fraction = (...n) => {
  return JSON.parse(JSON.stringify(WeightNumber(...n)))
}

describe('Gym()', () => {
  beforeEach(() => {
    subject = Gym()
  })

  it('should have no bars', () => {
    expect(subject.bars()).to.deep.equal([])
  })

  it('should have no targets', () => {
    expect(subject.targets()).to.deep.equal([])
  })
})

describe('Gym({ bars: [45] })', () => {
  beforeEach(() => {
    subject = Gym({ bars: [45] })
  })

  it('should have one bar', () => {
    expect(subject.bars()).to.have.lengthOf(1)
  })

  describe('.bars()[0]', () => {
    beforeEach(() => {
      subject = subject.bars()[0]
    })

    it('should have weight 45', () => {
      expect(subject.weight).to.deep.include(fraction(45))
    })

    it('should have targets [45]', () => {
      expect(subject.targets()).to.have.lengthOf(1)
      expect(subject.targets()[0]).to.deep.include(fraction(45))
    })
  })
})

describe('Gym({ bars: [60] })', () => {
  beforeEach(() => {
    subject = Gym({ bars: [60] })
  })

  it('should have one bar', () => {
    expect(subject.bars()).to.have.lengthOf(1)
  })

  describe('.bars()[0]', () => {
    beforeEach(() => {
      subject = subject.bars()[0]
    })

    it('should have weight 60', () => {
      expect(subject.weight).to.deep.include(fraction(60))
    })
  })
})

describe('Gym({ bars: [{ weight: 45, shape: "olympic" }, { weight: 60, shape: "hex" }] })', () => {
  beforeEach(() => {
    subject = Gym({ bars: [{ weight: 45, shape: 'olympic' }, { weight: 60, shape: 'hex' }] })
  })

  it('should have two bars', () => {
    expect(subject.bars()).to.have.lengthOf(2)
  })

  describe('.bars()[0]', () => {
    beforeEach(() => {
      subject = subject.bars()[0]
    })

    it('should have weight 45', () => {
      expect(subject.weight).to.deep.include(fraction(45))
    })
  })

  describe('.bars()[1]', () => {
    beforeEach(() => {
      subject = subject.bars()[1]
    })

    it('should have weight 60', () => {
      expect(subject.weight).to.deep.include(fraction(60))
    })
  })
})

describe('Gym({ bars: [45], plates: [5] })', () => {
  beforeEach(() => {
    subject = Gym({ bars: [45], plates: [5] })
  })

  it('should have one bar', () => {
    expect(subject.bars()).to.have.lengthOf(1)
  })

  describe('.bars()[0]', () => {
    beforeEach(() => {
      subject = subject.bars()[0]
    })

    it('should have weight 45', () => {
      expect(subject.weight).to.deep.include(fraction(45))
    })

    it('should have targets [45, 55]', () => {
      expect(subject.targets()).to.have.lengthOf(2)
      expect(subject.targets()[0]).to.deep.include(fraction(45))
      expect(subject.targets()[1]).to.deep.include(fraction(55))
    })
  })
})

describe('Gym({ bars: [60], plates: [5/2] })', () => {
  beforeEach(() => {
    subject = Gym({ bars: [60], plates: [{ numerator: 5, denominator: 2 }] })
  })

  it('should have one bar', () => {
    expect(subject.bars()).to.have.lengthOf(1)
  })

  describe('.bars()[0]', () => {
    beforeEach(() => {
      subject = subject.bars()[0]
    })

    it('should have weight 60', () => {
      expect(subject.weight).to.deep.include(fraction(60))
    })

    it('should have targets [60, 65]', () => {
      expect(subject.targets()).to.have.lengthOf(2)
      expect(subject.targets()[0]).to.deep.include(fraction(60))
      expect(subject.targets()[1]).to.deep.include(fraction(65))
    })
  })
})

describe('Gym({ bars: [45], plates: [5, 10] })', () => {
  beforeEach(() => {
    subject = Gym({ bars: [45], plates: [5, 10] })
  })

  it('should have one bar', () => {
    expect(subject.bars()).to.have.lengthOf(1)
  })

  describe('.bars()[0]', () => {
    beforeEach(() => {
      subject = subject.bars()[0]
    })

    it('should have weight 45', () => {
      expect(subject.weight).to.deep.include(fraction(45))
    })

    it('should have targets [45, 55, 65, 75]', () => {
      expect(subject.targets()).to.have.lengthOf(4)
      expect(subject.targets()[0]).to.deep.include(fraction(45))
      expect(subject.targets()[1]).to.deep.include(fraction(55))
      expect(subject.targets()[2]).to.deep.include(fraction(65))
      expect(subject.targets()[3]).to.deep.include(fraction(75))
    })
  })
})

describe('Gym({ bars: [45], plates: [5, 5] })', () => {
  beforeEach(() => {
    subject = Gym({ bars: [45], plates: [5, 5] })
  })

  it('should have one bar', () => {
    expect(subject.bars()).to.have.lengthOf(1)
  })

  describe('.bars()[0]', () => {
    beforeEach(() => {
      subject = subject.bars()[0]
    })

    it('should have weight 45', () => {
      expect(subject.weight).to.deep.include(fraction(45))
    })

    it('should have targets [45, 55, 65]', () => {
      expect(subject.targets()).to.have.lengthOf(3)
      expect(subject.targets()[0]).to.deep.include(fraction(45))
      expect(subject.targets()[1]).to.deep.include(fraction(55))
      expect(subject.targets()[2]).to.deep.include(fraction(65))
    })
  })
})

describe('Gym({ bars: [45], plates: [.25, .5, .75, 1, 2.5, 5, 5, 10, 25] })', () => {
  beforeEach(() => {
    subject = Gym({ bars: [45], plates: [0.25, 0.5, 0.75, 1, 2.5, 5, 5, 10, 25] })
  })

  it('should have one bar', () => {
    expect(subject.bars()).to.have.lengthOf(1)
  })

  describe('.bars()[0]', () => {
    beforeEach(() => {
      subject = subject.bars()[0]
    })

    it('should have weight 45', () => {
      expect(subject.weight).to.deep.include(fraction(45))
    })

    it('should have targets [45, 45.5, 46, ..., 144.5, 145]', () => {
      expect(subject.targets()).to.have.lengthOf(201)
      for (let i = 0; i <= 100; i += 1) {
        expect(subject.targets()[i]).to.deep.include(fraction({
          numerator: 90 + i,
          denominator: 2
        }))
      }
    })
  })
})

describe('Gym({ bars: [45], plates: [50x pairs of 25 and 45] }).bars()[0]', () => {
  beforeEach(() => {
    const plates = []
    for (let i = 0; i < 50; i += 1) {
      // Setting [25, 45, 25, 45, ...] instead of [25, 25, ..., 45, 45]
      // forces us to sort the plates.
      plates.push(25)
      plates.push(45)
    }

    subject = Gym({ bars: [45], plates }).bars()[0]
  })

  // Without the optimizations around duplicate plates, this test takes
  // minutes to complete, so a timeout of 1 second ensures we aren't
  // doing anything too stupid.
  it('should calculate targets quickly', { timeout: 1000 }, () => {
    expect(subject.targets()).not.to.have.lengthOf(0)
  })
})

describe('Gym({ bars: [60], plates: [5, 10] })', () => {
  beforeEach(() => {
    subject = Gym({ bars: [60], plates: [5, 10] })
  })

  describe('.bars()[0]', () => {
    beforeEach(() => {
      subject = subject.bars()[0]
    })

    describe('.loadFor(50)', () => {
      it('should raise an exception', () => {
        expect(() => {
          subject = subject.loadFor(50)
        }).to.throw()
      })
    })

    describe('.loadFor(60)', () => {
      beforeEach(() => {
        subject = subject.loadFor(60)
      })

      it('should be []', () => {
        expect(subject).to.deep.equal([])
      })
    })

    describe('.loadFor(70)', () => {
      beforeEach(() => {
        subject = subject.loadFor(70)
      })

      it('should be [5]', () => {
        expect(subject).to.have.lengthOf(1)
        expect(subject[0].weight).to.deep.include(fraction(5))
      })
    })

    describe('.loadFor(80)', () => {
      beforeEach(() => {
        subject = subject.loadFor(80)
      })

      it('should be [10]', () => {
        expect(subject).to.have.lengthOf(1)
        expect(subject[0].weight).to.deep.include(fraction(10))
      })
    })

    describe('.loadFor(90)', () => {
      beforeEach(() => {
        subject = subject.loadFor(90)
      })

      it('should be [10, 5]', () => {
        expect(subject).to.have.lengthOf(2)
        expect(subject[0].weight).to.deep.include(fraction(10))
        expect(subject[1].weight).to.deep.include(fraction(5))
      })
    })

    describe('.loadFor(100)', () => {
      it('should raise an exception', () => {
        expect(() => {
          subject = subject.loadFor(100)
        }).to.throw()
      })
    })

    it('.nearest(50) should be 60', () => {
      expect(subject.nearest(50)).to.deep.include(fraction(60))
    })

    it('.nearest(60) should be 60', () => {
      expect(subject.nearest(60)).to.deep.include(fraction(60))
    })

    it('.nearest(64) should be 60', () => {
      expect(subject.nearest(64)).to.deep.include(fraction(60))
    })

    it('.nearest(65) should be 60', () => {
      expect(subject.nearest(65)).to.deep.include(fraction(60))
    })

    it('.nearest(66) should be 70', () => {
      expect(subject.nearest(66)).to.deep.include(fraction(70))
    })

    it('.nearest(100) should be 90', () => {
      expect(subject.nearest(100)).to.deep.include(fraction(90))
    })

    it('.roundDown(50) should be 60', () => {
      expect(subject.roundDown(50)).to.deep.include(fraction(60))
    })

    it('.roundDown(60) should be 60', () => {
      expect(subject.roundDown(60)).to.deep.include(fraction(60))
    })

    it('.roundDown(64) should be 60', () => {
      expect(subject.roundDown(64)).to.deep.include(fraction(60))
    })

    it('.roundDown(65) should be 60', () => {
      expect(subject.roundDown(65)).to.deep.include(fraction(60))
    })

    it('.roundDown(66) should be 60', () => {
      expect(subject.roundDown(66)).to.deep.include(fraction(60))
    })

    it('.roundDown(100) should be 90', () => {
      expect(subject.roundDown(100)).to.deep.include(fraction(90))
    })

    it('.roundUp(50) should be 60', () => {
      expect(subject.roundUp(50)).to.deep.include(fraction(60))
    })

    it('.roundUp(60) should be 60', () => {
      expect(subject.roundUp(60)).to.deep.include(fraction(60))
    })

    it('.roundUp(64) should be 70', () => {
      expect(subject.roundUp(64)).to.deep.include(fraction(70))
    })

    it('.roundUp(65) should be 70', () => {
      expect(subject.roundUp(65)).to.deep.include(fraction(70))
    })

    it('.roundUp(66) should be 70', () => {
      expect(subject.roundUp(66)).to.deep.include(fraction(70))
    })

    it('.roundUp(100) should be 90', () => {
      expect(subject.roundUp(100)).to.deep.include(fraction(90))
    })

    it('.plan(60) should be [60]', () => {
      expect(subject.plan(60)).to.have.lengthOf(1)
      expect(subject.plan(60)[0].weight).to.deep.include(fraction(60))
    })

    it('.plan(70) should be [60, 70]', () => {
      expect(subject.plan(70)).to.have.lengthOf(2)
      expect(subject.plan(70)[0].weight).to.deep.include(fraction(60))
      expect(subject.plan(70)[1].weight).to.deep.include(fraction(70))
    })

    it('.plan(80) should be [60, 80]', () => {
      expect(subject.plan(80)).to.have.lengthOf(2)
      expect(subject.plan(80)[0].weight).to.deep.include(fraction(60))
      expect(subject.plan(80)[1].weight).to.deep.include(fraction(80))
    })

    it('.plan(90) should be [60, 90]', () => {
      expect(subject.plan(90)).to.have.lengthOf(2)
      expect(subject.plan(90)[0].weight).to.deep.include(fraction(60))
      expect(subject.plan(90)[1].weight).to.deep.include(fraction(90))
    })

    it('.plan(90, { maxStep: 10 }) should be [60, 70, 80, 90]', () => {
      expect(subject.plan(90, { maxStep: 10 })).to.have.lengthOf(4)
      expect(subject.plan(90, { maxStep: 10 })[0].weight).to.deep.include(fraction(60))
      expect(subject.plan(90, { maxStep: 10 })[1].weight).to.deep.include(fraction(70))
      expect(subject.plan(90, { maxStep: 10 })[2].weight).to.deep.include(fraction(80))
      expect(subject.plan(90, { maxStep: 10 })[3].weight).to.deep.include(fraction(90))
    })
  })
})

// This is my gym! I hope to someday add some more barbells, like maybe
// an open trap bar and one of those cushioned squat bars, and maybe
// even a 15lb training bar... but I have limited space right now, so
// it's one barbell, some pulls, and a folding wall rack with diy
// spotters improvised out of climbing rope. Thank you for reading this.
describe('Gym({ bars: [{ weight: 45, type: "olympic" }, { weight: 70, type: "olympic+pulls" }], plates: [.25, .5, .75, 1, 2.5, 5, 5, 10, 25, 35, 45, 45] })', () => {
  beforeEach(() => {
    subject = Gym({
      bars: [{
        weight: 45,
        type: 'olympic',
        id: 1
      }, {
        weight: 70,
        type: 'olympic+pulls',
        id: 2,
        ifTooHeavy: 1
      }],
      plates: [0.25, 0.5, 0.75, 1, 2.5, 5, 5, 10, 25, 35, 45, 45]
    })
  })

  describe('.barById(1)', () => {
    beforeEach(() => {
      subject = subject.barById(1)
    })

    describe('.loadFor(40)', () => {
      it('should raise an exception', () => {
        expect(() => {
          subject = subject.loadFor(40)
        }).to.throw()
      })
    })

    const table = [
      { target: 45.0, load: [] },
      { target: 45.5, load: [0.25] },
      { target: 46.0, load: [0.5] },
      { target: 46.5, load: [0.75] },
      { target: 47.0, load: [1] },
      { target: 47.5, load: [0.75, 0.5] }, // could be 1, .25
      { target: 48.0, load: [1, 0.5] },
      { target: 48.5, load: [1, 0.75] },
      { target: 49.0, load: [1, 0.75, 0.25] },
      { target: 49.5, load: [1, 0.75, 0.5] },
      { target: 50.0, load: [2.5] },
      { target: 50.5, load: [2.5, 0.25] },
      { target: 51.0, load: [2.5, 0.5] },
      { target: 51.5, load: [2.5, 0.75] },
      { target: 52.0, load: [2.5, 1] },
      { target: 52.5, load: [2.5, 0.75, 0.5] }, // could end with 1, .25
      { target: 53.0, load: [2.5, 1, 0.5] },
      { target: 53.5, load: [2.5, 1, 0.75] },
      { target: 54.0, load: [2.5, 1, 0.75, 0.25] },
      { target: 54.5, load: [2.5, 1, 0.75, 0.5] },
      { target: 55.0, load: [5] },
      { target: 55.5, load: [5, 0.25] },
      { target: 56.0, load: [5, 0.5] },
      { target: 56.5, load: [5, 0.75] },
      { target: 57.0, load: [5, 1] },
      { target: 57.5, load: [5, 0.75, 0.5] }, // could end with 1, .25
      { target: 58.0, load: [5, 1, 0.5] },
      { target: 58.5, load: [5, 1, 0.75] },
      { target: 59.0, load: [5, 1, 0.75, 0.25] },
      { target: 59.5, load: [5, 1, 0.75, 0.5] },
      { target: 60.0, load: [5, 2.5] },
      { target: 60.5, load: [5, 2.5, 0.25] },
      { target: 61.0, load: [5, 2.5, 0.5] },
      { target: 61.5, load: [5, 2.5, 0.75] },
      { target: 62.0, load: [5, 2.5, 1] },
      { target: 62.5, load: [5, 2.5, 0.75, 0.5] }, // could end with 1, .25
      { target: 63.0, load: [5, 2.5, 1, 0.5] },
      { target: 63.5, load: [5, 2.5, 1, 0.75] },
      { target: 64.0, load: [5, 2.5, 1, 0.75, 0.25] },
      { target: 64.5, load: [5, 2.5, 1, 0.75, 0.5] },
      { target: 65.0, load: [10] }
    ]

    table.forEach(o => {
      describe(`.loadFor(${o.target})`, () => {
        beforeEach(() => {
          subject = subject.loadFor(o.target)
        })

        it(`should be ${JSON.stringify(o.load)}`, () => {
          expect(subject).to.have.lengthOf(o.load.length)
          for (let i = 0; i < o.load.length; i += 1) {
            expect(subject[i].weight).to.deep.include(fraction(o.load[i]))
          }
        })
      })
    })

    describe('.loadsFor([45, 56.5, 68, 79, 90])', () => {
      beforeEach(() => {
        subject = subject.loadsFor([45, 56.5, 68, 79, 90])
      })

      it('should have step 1 be an empty bar', () => {
        expect(subject[0]).to.have.lengthOf(0)
      })

      it('should have step 2 be [5, 0.75]', () => {
        expect(subject[1]).to.have.lengthOf(2)
        expect(subject[1][0].weight).to.deep.include(fraction(5))
        expect(subject[1][1].weight).to.deep.include(fraction(0.75))
      })

      it('should have step 3 be [10, 1, 0.5]', () => {
        expect(subject[2]).to.have.lengthOf(3)
        expect(subject[2][0].weight).to.deep.include(fraction(10))
        expect(subject[2][1].weight).to.deep.include(fraction(1))
        expect(subject[2][2].weight).to.deep.include(fraction(0.5))
      })

      it('should have step 4 be [10, 5, 1, 0.75, 0.25]', () => {
        expect(subject[3]).to.have.lengthOf(5)
        expect(subject[3][0].weight).to.deep.include(fraction(10))
        expect(subject[3][1].weight).to.deep.include(fraction(5))
        expect(subject[3][2].weight).to.deep.include(fraction(1))
        expect(subject[3][3].weight).to.deep.include(fraction(0.75))
        expect(subject[3][4].weight).to.deep.include(fraction(0.25))
      })

      it('should have step 5 be [10, 5, 5, 2.5]', () => {
        expect(subject[4]).to.have.lengthOf(4)
        expect(subject[4][0].weight).to.deep.include(fraction(10))
        expect(subject[4][1].weight).to.deep.include(fraction(5))
        expect(subject[4][2].weight).to.deep.include(fraction(5))
        expect(subject[4][3].weight).to.deep.include(fraction(2.5))
      })
    })

    describe('.loadsFor([45, 70, 95, 120, 145])', () => {
      beforeEach(() => {
        subject = subject.loadsFor([45, 70, 95, 120, 145])
      })

      it('should have step 1 be an empty bar', () => {
        expect(subject[0]).to.have.lengthOf(0)
      })

      it('should have step 2 be [10, 2.5]', () => {
        expect(subject[1]).to.have.lengthOf(2)
        expect(subject[1][0].weight).to.deep.include(fraction(10))
        expect(subject[1][1].weight).to.deep.include(fraction(2.5))
      })

      it('should have step 3 be [25]', () => {
        expect(subject[2]).to.have.lengthOf(1)
        expect(subject[2][0].weight).to.deep.include(fraction(25))
      })

      it('should have step 4 be [25, 10, 2.5]', () => {
        expect(subject[3]).to.have.lengthOf(3)
        expect(subject[3][0].weight).to.deep.include(fraction(25))
        expect(subject[3][1].weight).to.deep.include(fraction(10))
        expect(subject[3][2].weight).to.deep.include(fraction(2.5))
      })

      it('should have step 5 be [25, 10, 5, 5, 2.5, 1, .75, .5, .25]', () => {
        expect(subject[4]).to.have.lengthOf(9)
        expect(subject[4][0].weight).to.deep.include(fraction(25))
        expect(subject[4][1].weight).to.deep.include(fraction(10))
        expect(subject[4][2].weight).to.deep.include(fraction(5))
        expect(subject[4][3].weight).to.deep.include(fraction(5))
        expect(subject[4][4].weight).to.deep.include(fraction(2.5))
        expect(subject[4][5].weight).to.deep.include(fraction(1))
        expect(subject[4][6].weight).to.deep.include(fraction(0.75))
        expect(subject[4][7].weight).to.deep.include(fraction(0.5))
        expect(subject[4][8].weight).to.deep.include(fraction(0.25))
      })
    })

    describe('.loadsFor([45, 87.5, 130, 172.5, 215])', () => {
      beforeEach(() => {
        subject = subject.loadsFor([45, 87.5, 130, 172.5, 215])
      })

      it('should have step 1 be an empty bar', () => {
        expect(subject[0]).to.have.lengthOf(0)
      })

      it('should have step 2 be [10, 5, 5, .75, .5]', () => {
        expect(subject[1]).to.have.lengthOf(5)
        expect(subject[1][0].weight).to.deep.include(fraction(10))
        expect(subject[1][1].weight).to.deep.include(fraction(5))
        expect(subject[1][2].weight).to.deep.include(fraction(5))
        expect(subject[1][3].weight).to.deep.include(fraction(0.75))
        expect(subject[1][4].weight).to.deep.include(fraction(0.5))
      })

      it('should have step 3 be [35, 5, 2.5]', () => {
        expect(subject[2]).to.have.lengthOf(3)
        expect(subject[2][0].weight).to.deep.include(fraction(35))
        expect(subject[2][1].weight).to.deep.include(fraction(5))
        expect(subject[2][2].weight).to.deep.include(fraction(2.5))
      })

      it('should have step 4 be [35, 25, 2.5, .75, .5]', () => {
        expect(subject[3]).to.have.lengthOf(5)
        expect(subject[3][0].weight).to.deep.include(fraction(35))
        expect(subject[3][1].weight).to.deep.include(fraction(25))
        expect(subject[3][2].weight).to.deep.include(fraction(2.5))
        expect(subject[3][3].weight).to.deep.include(fraction(0.75))
        expect(subject[3][4].weight).to.deep.include(fraction(0.5))
      })

      it('should have step 5 be [35, 25, 10, 5, 5, 2.5, 1, .75, .5, .25]', () => {
        expect(subject[4]).to.have.lengthOf(10)
        expect(subject[4][0].weight).to.deep.include(fraction(35))
        expect(subject[4][1].weight).to.deep.include(fraction(25))
        expect(subject[4][2].weight).to.deep.include(fraction(10))
        expect(subject[4][3].weight).to.deep.include(fraction(5))
        expect(subject[4][4].weight).to.deep.include(fraction(5))
        expect(subject[4][5].weight).to.deep.include(fraction(2.5))
        expect(subject[4][6].weight).to.deep.include(fraction(1))
        expect(subject[4][7].weight).to.deep.include(fraction(0.75))
        expect(subject[4][8].weight).to.deep.include(fraction(0.5))
        expect(subject[4][9].weight).to.deep.include(fraction(0.25))
      })
    })

    describe('.loadsFor([45, 89, 133, 176.5, 220])', () => {
      beforeEach(() => {
        subject = subject.loadsFor([45, 89, 133, 176.5, 220])
      })

      it('should have step 1 be an empty bar', () => {
        expect(subject[0]).to.have.lengthOf(0)
      })

      it('should have step 2 be [10, 5, 5, 1, .75, .25]', () => {
        expect(subject[1]).to.have.lengthOf(6)
        expect(subject[1][0].weight).to.deep.include(fraction(10))
        expect(subject[1][1].weight).to.deep.include(fraction(5))
        expect(subject[1][2].weight).to.deep.include(fraction(5))
        expect(subject[1][3].weight).to.deep.include(fraction(1))
        expect(subject[1][4].weight).to.deep.include(fraction(0.75))
        expect(subject[1][5].weight).to.deep.include(fraction(0.25))
      })

      it('should have step 3 be [35, 5, 2.5, 1, .5]', () => {
        expect(subject[2]).to.have.lengthOf(5)
        expect(subject[2][0].weight).to.deep.include(fraction(35))
        expect(subject[2][1].weight).to.deep.include(fraction(5))
        expect(subject[2][2].weight).to.deep.include(fraction(2.5))
        expect(subject[2][3].weight).to.deep.include(fraction(1))
        expect(subject[2][4].weight).to.deep.include(fraction(0.5))
      })

      it('should have step 4 be [45, 10, 5, 5, .75]', () => {
        expect(subject[3]).to.have.lengthOf(5)
        expect(subject[3][0].weight).to.deep.include(fraction(45))
        expect(subject[3][1].weight).to.deep.include(fraction(10))
        expect(subject[3][2].weight).to.deep.include(fraction(5))
        expect(subject[3][3].weight).to.deep.include(fraction(5))
        expect(subject[3][4].weight).to.deep.include(fraction(0.75))
      })

      it('should have step 5 be [45, 35, 5, 2.5]', () => {
        expect(subject[4]).to.have.lengthOf(4)
        expect(subject[4][0].weight).to.deep.include(fraction(45))
        expect(subject[4][1].weight).to.deep.include(fraction(35))
        expect(subject[4][2].weight).to.deep.include(fraction(5))
        expect(subject[4][3].weight).to.deep.include(fraction(2.5))
      })
    })

    it('.plan(90) should be [45, 90]', () => {
      expect(subject.plan(90)).to.have.lengthOf(2)
      expect(subject.plan(90)[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(90)[1].weight).to.deep.include(fraction(90))
    })

    it('.plan(91) should be [45, 68, 91]', () => {
      expect(subject.plan(91)).to.have.lengthOf(3)
      expect(subject.plan(91)[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(91)[1].weight).to.deep.include(fraction(68))
      expect(subject.plan(91)[2].weight).to.deep.include(fraction(91))
    })

    it('.plan(180) should be [45, 90, 135, 180]', () => {
      expect(subject.plan(180)).to.have.lengthOf(4)
      expect(subject.plan(180)[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(180)[1].weight).to.deep.include(fraction(90))
      expect(subject.plan(180)[2].weight).to.deep.include(fraction(135))
      expect(subject.plan(180)[3].weight).to.deep.include(fraction(180))
    })

    it('.plan(180.5) should be [45, 79, 113, 147, 180.5]', () => {
      expect(subject.plan(180.5)).to.have.lengthOf(5)
      expect(subject.plan(180.5)[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(180.5)[1].weight).to.deep.include(fraction(79))
      expect(subject.plan(180.5)[2].weight).to.deep.include(fraction(113))
      expect(subject.plan(180.5)[3].weight).to.deep.include(fraction(147))
      expect(subject.plan(180.5)[4].weight).to.deep.include(fraction(180.5))
    })

    it('.plan(181) should be [45, 79, 113, 147, 181]', () => {
      expect(subject.plan(181)).to.have.lengthOf(5)
      expect(subject.plan(181)[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(181)[1].weight).to.deep.include(fraction(79))
      expect(subject.plan(181)[2].weight).to.deep.include(fraction(113))
      expect(subject.plan(181)[3].weight).to.deep.include(fraction(147))
      expect(subject.plan(181)[4].weight).to.deep.include(fraction(181))
    })

    it('.plan(181.5) should be [45, 79.5, 113.5, 147.5, 181.5]', () => {
      expect(subject.plan(181.5)).to.have.lengthOf(5)
      expect(subject.plan(181.5)[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(181.5)[1].weight).to.deep.include(fraction(79.5))
      expect(subject.plan(181.5)[2].weight).to.deep.include(fraction(113.5))
      expect(subject.plan(181.5)[3].weight).to.deep.include(fraction(147.5))
      expect(subject.plan(181.5)[4].weight).to.deep.include(fraction(181.5))
    })

    it('.plan(182) should be [45, 79.5, 114, 148, 182]', () => {
      expect(subject.plan(182)).to.have.lengthOf(5)
      expect(subject.plan(182)[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(182)[1].weight).to.deep.include(fraction(79.5))
      expect(subject.plan(182)[2].weight).to.deep.include(fraction(114))
      expect(subject.plan(182)[3].weight).to.deep.include(fraction(148))
      expect(subject.plan(182)[4].weight).to.deep.include(fraction(182))
    })

    it('.plan(182.5) should be [45, 79.5, 114, 148.5, 182.5]', () => {
      expect(subject.plan(182.5)).to.have.lengthOf(5)
      expect(subject.plan(182.5)[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(182.5)[1].weight).to.deep.include(fraction(79.5))
      expect(subject.plan(182.5)[2].weight).to.deep.include(fraction(114))
      expect(subject.plan(182.5)[3].weight).to.deep.include(fraction(148.5))
      expect(subject.plan(182.5)[4].weight).to.deep.include(fraction(182.5))
    })

    it('.plan(183) should be [45, 79.5, 114, 148.5, 183]', () => {
      expect(subject.plan(183)).to.have.lengthOf(5)
      expect(subject.plan(183)[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(183)[1].weight).to.deep.include(fraction(79.5))
      expect(subject.plan(183)[2].weight).to.deep.include(fraction(114))
      expect(subject.plan(183)[3].weight).to.deep.include(fraction(148.5))
      expect(subject.plan(183)[4].weight).to.deep.include(fraction(183))
    })

    it('.plan(183.5) should be [45, 80, 114.5, 149, 183.5]', () => {
      expect(subject.plan(183.5)).to.have.lengthOf(5)
      expect(subject.plan(183.5)[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(183.5)[1].weight).to.deep.include(fraction(80))
      expect(subject.plan(183.5)[2].weight).to.deep.include(fraction(114.5))
      expect(subject.plan(183.5)[3].weight).to.deep.include(fraction(149))
      expect(subject.plan(183.5)[4].weight).to.deep.include(fraction(183.5))
    })

    it('.plan(184) should be [45, 80, 115, 149.5, 184]', () => {
      expect(subject.plan(184)).to.have.lengthOf(5)
      expect(subject.plan(184)[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(184)[1].weight).to.deep.include(fraction(80))
      expect(subject.plan(184)[2].weight).to.deep.include(fraction(115))
      expect(subject.plan(184)[3].weight).to.deep.include(fraction(149.5))
      expect(subject.plan(184)[4].weight).to.deep.include(fraction(184))
    })

    it('.plan(184.5) should be [45, 80, 115, 150, 184.5]', () => {
      expect(subject.plan(184.5)).to.have.lengthOf(5)
      expect(subject.plan(184.5)[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(184.5)[1].weight).to.deep.include(fraction(80))
      expect(subject.plan(184.5)[2].weight).to.deep.include(fraction(115))
      expect(subject.plan(184.5)[3].weight).to.deep.include(fraction(150))
      expect(subject.plan(184.5)[4].weight).to.deep.include(fraction(184.5))
    })

    it('.plan(185) should be [45, 80, 115, 150, 185]', () => {
      expect(subject.plan(185)).to.have.lengthOf(5)
      expect(subject.plan(185)[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(185)[1].weight).to.deep.include(fraction(80))
      expect(subject.plan(185)[2].weight).to.deep.include(fraction(115))
      expect(subject.plan(185)[3].weight).to.deep.include(fraction(150))
      expect(subject.plan(185)[4].weight).to.deep.include(fraction(185))
    })

    it('.plan(45) should be [45]', () => {
      expect(subject.plan(45)).to.have.lengthOf(1)
      expect(subject.plan(45)[0].weight).to.deep.include(fraction(45))
    })

    it('.plan(45, { minSteps: 5 }) should be [45, 45, 45, 45, 45]', () => {
      expect(subject.plan(45, { minSteps: 5 })).to.have.lengthOf(5)
      expect(subject.plan(45, { minSteps: 5 })[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(45, { minSteps: 5 })[1].weight).to.deep.include(fraction(45))
      expect(subject.plan(45, { minSteps: 5 })[2].weight).to.deep.include(fraction(45))
      expect(subject.plan(45, { minSteps: 5 })[3].weight).to.deep.include(fraction(45))
      expect(subject.plan(45, { minSteps: 5 })[4].weight).to.deep.include(fraction(45))
    })

    it('.plan(45.5, { minSteps: 5 }) should be [45, 45.5, 45.5, 45.5, 45.5]', () => {
      expect(subject.plan(45.5, { minSteps: 5 })).to.have.lengthOf(5)
      expect(subject.plan(45.5, { minSteps: 5 })[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(45.5, { minSteps: 5 })[1].weight).to.deep.include(fraction(45.5))
      expect(subject.plan(45.5, { minSteps: 5 })[2].weight).to.deep.include(fraction(45.5))
      expect(subject.plan(45.5, { minSteps: 5 })[3].weight).to.deep.include(fraction(45.5))
      expect(subject.plan(45.5, { minSteps: 5 })[4].weight).to.deep.include(fraction(45.5))
    })

    it('.plan(46, { minSteps: 5 }) should be [45, 45.5, 46, 46, 46]', () => {
      expect(subject.plan(46, { minSteps: 5 })).to.have.lengthOf(5)
      expect(subject.plan(46, { minSteps: 5 })[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(46, { minSteps: 5 })[1].weight).to.deep.include(fraction(45.5))
      expect(subject.plan(46, { minSteps: 5 })[2].weight).to.deep.include(fraction(46))
      expect(subject.plan(46, { minSteps: 5 })[3].weight).to.deep.include(fraction(46))
      expect(subject.plan(46, { minSteps: 5 })[4].weight).to.deep.include(fraction(46))
    })

    it('.plan(46.5, { minSteps: 5 }) should be [45, 45.5, 46, 46.5, 46.5]', () => {
      expect(subject.plan(46.5, { minSteps: 5 })).to.have.lengthOf(5)
      expect(subject.plan(46.5, { minSteps: 5 })[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(46.5, { minSteps: 5 })[1].weight).to.deep.include(fraction(45.5))
      expect(subject.plan(46.5, { minSteps: 5 })[2].weight).to.deep.include(fraction(46))
      expect(subject.plan(46.5, { minSteps: 5 })[3].weight).to.deep.include(fraction(46.5))
      expect(subject.plan(46.5, { minSteps: 5 })[4].weight).to.deep.include(fraction(46.5))
    })

    it('.plan(47, { minSteps: 5 }) should be [45, 45.5, 46, 46.5, 47]', () => {
      expect(subject.plan(47, { minSteps: 5 })).to.have.lengthOf(5)
      expect(subject.plan(47, { minSteps: 5 })[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(47, { minSteps: 5 })[1].weight).to.deep.include(fraction(45.5))
      expect(subject.plan(47, { minSteps: 5 })[2].weight).to.deep.include(fraction(46))
      expect(subject.plan(47, { minSteps: 5 })[3].weight).to.deep.include(fraction(46.5))
      expect(subject.plan(47, { minSteps: 5 })[4].weight).to.deep.include(fraction(47))
    })

    it('.plan(47.5, { minSteps: 5 }) should be [45, 46, 46.5, 47, 47.5]', () => {
      expect(subject.plan(47.5, { minSteps: 5 })).to.have.lengthOf(5)
      expect(subject.plan(47.5, { minSteps: 5 })[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(47.5, { minSteps: 5 })[1].weight).to.deep.include(fraction(46))
      expect(subject.plan(47.5, { minSteps: 5 })[2].weight).to.deep.include(fraction(46.5))
      expect(subject.plan(47.5, { minSteps: 5 })[3].weight).to.deep.include(fraction(47))
      expect(subject.plan(47.5, { minSteps: 5 })[4].weight).to.deep.include(fraction(47.5))
    })

    it('.plan(48, { minSteps: 5 }) should be [45, 46, 47, 47.5, 48]', () => {
      expect(subject.plan(48, { minSteps: 5 })).to.have.lengthOf(5)
      expect(subject.plan(48, { minSteps: 5 })[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(48, { minSteps: 5 })[1].weight).to.deep.include(fraction(46))
      expect(subject.plan(48, { minSteps: 5 })[2].weight).to.deep.include(fraction(47))
      expect(subject.plan(48, { minSteps: 5 })[3].weight).to.deep.include(fraction(47.5))
      expect(subject.plan(48, { minSteps: 5 })[4].weight).to.deep.include(fraction(48))
    })

    it('.plan(48.5, { minSteps: 5 }) should be [45, 46, 47, 48, 48.5]', () => {
      expect(subject.plan(48.5, { minSteps: 5 })).to.have.lengthOf(5)
      expect(subject.plan(48.5, { minSteps: 5 })[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(48.5, { minSteps: 5 })[1].weight).to.deep.include(fraction(46))
      expect(subject.plan(48.5, { minSteps: 5 })[2].weight).to.deep.include(fraction(47))
      expect(subject.plan(48.5, { minSteps: 5 })[3].weight).to.deep.include(fraction(48))
      expect(subject.plan(48.5, { minSteps: 5 })[4].weight).to.deep.include(fraction(48.5))
    })

    it('.plan(49, { minSteps: 5 }) should be [45, 46, 47, 48, 49]', () => {
      expect(subject.plan(49, { minSteps: 5 })).to.have.lengthOf(5)
      expect(subject.plan(49, { minSteps: 5 })[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(49, { minSteps: 5 })[1].weight).to.deep.include(fraction(46))
      expect(subject.plan(49, { minSteps: 5 })[2].weight).to.deep.include(fraction(47))
      expect(subject.plan(49, { minSteps: 5 })[3].weight).to.deep.include(fraction(48))
      expect(subject.plan(49, { minSteps: 5 })[4].weight).to.deep.include(fraction(49))
    })

    it('.plan(49, { minSteps: 5 }) should all use the id:1 bar', () => {
      expect(subject.plan(49, { minSteps: 5 })[0].bar.id).to.equal(1)
      expect(subject.plan(49, { minSteps: 5 })[1].bar.id).to.equal(1)
      expect(subject.plan(49, { minSteps: 5 })[2].bar.id).to.equal(1)
      expect(subject.plan(49, { minSteps: 5 })[3].bar.id).to.equal(1)
      expect(subject.plan(49, { minSteps: 5 })[4].bar.id).to.equal(1)
    })
  })

  describe('.barById(2)', () => {
    beforeEach(() => {
      subject = subject.barById(2)
    })

    it('.plan(85, { minSteps: 5 }) should be [70, 71, 72, 73, 74]', () => {
      expect(subject.plan(85, { minSteps: 5 })).to.have.lengthOf(5)
      expect(subject.plan(85, { minSteps: 5 })[0].weight).to.deep.include(fraction(70))
      expect(subject.plan(85, { minSteps: 5 })[1].weight).to.deep.include(fraction(74))
      expect(subject.plan(85, { minSteps: 5 })[2].weight).to.deep.include(fraction(78))
      expect(subject.plan(85, { minSteps: 5 })[3].weight).to.deep.include(fraction(81.5))
      expect(subject.plan(85, { minSteps: 5 })[4].weight).to.deep.include(fraction(85))
    })

    it('.plan(85, { minSteps: 5 }) should all use the id:2 bar', () => {
      expect(subject.plan(85, { minSteps: 5 })[0].bar.id).to.equal(2)
      expect(subject.plan(85, { minSteps: 5 })[1].bar.id).to.equal(2)
      expect(subject.plan(85, { minSteps: 5 })[2].bar.id).to.equal(2)
      expect(subject.plan(85, { minSteps: 5 })[3].bar.id).to.equal(2)
      expect(subject.plan(85, { minSteps: 5 })[4].bar.id).to.equal(2)
    })

    it('.plan(85, { minSteps: 5, startWeight: 45 }) should be [45, 55, 65, 75, 85]', () => {
      expect(subject.plan(85, { minSteps: 5, startWeight: 45 })).to.have.lengthOf(5)
      expect(subject.plan(85, { minSteps: 5, startWeight: 45 })[0].weight).to.deep.include(fraction(45))
      expect(subject.plan(85, { minSteps: 5, startWeight: 45 })[1].weight).to.deep.include(fraction(55))
      expect(subject.plan(85, { minSteps: 5, startWeight: 45 })[2].weight).to.deep.include(fraction(65))
      expect(subject.plan(85, { minSteps: 5, startWeight: 45 })[3].weight).to.deep.include(fraction(75))
      expect(subject.plan(85, { minSteps: 5, startWeight: 45 })[4].weight).to.deep.include(fraction(85))
    })

    it('.plan(85, { minSteps: 5, startWeight: 45 }) should all use the id:1 bar while below 70', () => {
      expect(subject.plan(85, { minSteps: 5, startWeight: 45 })[0].bar.id).to.equal(1)
      expect(subject.plan(85, { minSteps: 5, startWeight: 45 })[1].bar.id).to.equal(1)
      expect(subject.plan(85, { minSteps: 5, startWeight: 45 })[2].bar.id).to.equal(1)
      expect(subject.plan(85, { minSteps: 5, startWeight: 45 })[3].bar.id).to.equal(2)
      expect(subject.plan(85, { minSteps: 5, startWeight: 45 })[4].bar.id).to.equal(2)
    })
  })
})

// This was my first home gym! Everything was used and cost $0.10/lb.
// Before long, I did acquire some 2.5lb change plates and another pair
// of 10lb plates, but in this case, I would like to build tests around
// a gym whose possible target weights are not evenly distributed.
//
// In this case, for example,
//
// | 10 lbs | empty bar                      |
// | 20 lbs | bar plus 2x 5lb plates         |
// | 30 lbs | bar plus 2x 10lb plates        |
// | 40 lbs | bar plus 5lb and 10lb plates   |
// | 55 lbs | bar plus 2x 22.5lb plates      |
// | 65 lbs | bar plus 5lb and 22.5lb plates |
//
// So we can do 10, 20, 30, and 40, but not 50. We can do 55 and 65 but
// not 45 or 35. If this is you, lol I don't recommend this, but I do
// want to allow for it!
describe('Gym({ bars: [10], plates: [5, 10, 22.5, 22.5] })', () => {
  beforeEach(() => {
    subject = Gym({ bars: [10], plates: [5, 10, 22.5, 22.5] })
  })

  describe('.bars()[0]', () => {
    beforeEach(() => {
      subject = subject.bars()[0]
    })

    it('.plan(10, { maxStep: 20 }) should be [10]', () => {
      expect(subject.plan(10, { maxStep: 20 })).to.have.lengthOf(1)
      expect(subject.plan(10, { maxStep: 20 })[0].weight).to.deep.include(fraction(10))
    })

    it('.plan(20, { maxStep: 20 }) should be [10, 20]', () => {
      expect(subject.plan(20, { maxStep: 20 })).to.have.lengthOf(2)
      expect(subject.plan(20, { maxStep: 20 })[0].weight).to.deep.include(fraction(10))
      expect(subject.plan(20, { maxStep: 20 })[1].weight).to.deep.include(fraction(20))
    })

    it('.plan(30, { maxStep: 20 }) should be [10, 30]', () => {
      expect(subject.plan(30, { maxStep: 20 })).to.have.lengthOf(2)
      expect(subject.plan(30, { maxStep: 20 })[0].weight).to.deep.include(fraction(10))
      expect(subject.plan(30, { maxStep: 20 })[1].weight).to.deep.include(fraction(30))
    })

    it('.plan(40, { maxStep: 20 }) should be [10, 30, 40]', () => {
      expect(subject.plan(40, { maxStep: 20 })).to.have.lengthOf(3)
      expect(subject.plan(40, { maxStep: 20 })[0].weight).to.deep.include(fraction(10))
      expect(subject.plan(40, { maxStep: 20 })[1].weight).to.deep.include(fraction(30))
      expect(subject.plan(40, { maxStep: 20 })[2].weight).to.deep.include(fraction(40))
    })

    it('.plan(50, { maxStep: 20 }) should be [10, 30, 40, 55]', () => {
      expect(subject.plan(50, { maxStep: 20 })).to.have.lengthOf(4)
      expect(subject.plan(50, { maxStep: 20 })[0].weight).to.deep.include(fraction(10))
      expect(subject.plan(50, { maxStep: 20 })[1].weight).to.deep.include(fraction(30))
      expect(subject.plan(50, { maxStep: 20 })[2].weight).to.deep.include(fraction(40))
      expect(subject.plan(50, { maxStep: 20 })[3].weight).to.deep.include(fraction(55))
    })

    it('.plan(100, { maxStep: 30 }) should be [10, 30, 55, 75, 100]', () => {
      expect(subject.plan(100, { maxStep: 30 })).to.have.lengthOf(5)
      expect(subject.plan(100, { maxStep: 30 })[0].weight).to.deep.include(fraction(10))
      expect(subject.plan(100, { maxStep: 30 })[1].weight).to.deep.include(fraction(30))
      expect(subject.plan(100, { maxStep: 30 })[2].weight).to.deep.include(fraction(55))
      expect(subject.plan(100, { maxStep: 30 })[3].weight).to.deep.include(fraction(75))
      expect(subject.plan(100, { maxStep: 30 })[4].weight).to.deep.include(fraction(100))
    })
  })
})
