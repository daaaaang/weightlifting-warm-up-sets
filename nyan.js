const stats = {
  tick: 0,
  pass: 0,
  fail: 0,
  todo: 0,
  skip: 0,
  nest: [],
  failureDetails: [],
  todoDetails: []
}

const termWidth = process.stdout.getWindowSize(1)[0]
const width = Math.min(termWidth * 0.75, termWidth - 20)
const localNum = new Intl.NumberFormat()
const line = []
const is256 = true
//  [154, 154, 154, 184, 184, 214, 214,
//   208, 208, 202, 203, 203, 198, 198,
//   199, 163, 164, 128, 128,  93,  93,
//    57,  63,  63,  33,  33,  39,  39,
//    45,  44,  44,  49,  49,  48,  84,
//    83, 119, 118, 154, 154, 190, 184]
const colors = is256
  ? [154, 154, 154, 190, 184, 184, 214, 214, 208, 208, 202, 203, 203, 198, 198, 199, 163, 164, 128, 128, 93, 93, 57, 63, 63, 33, 33, 39, 39, 45, 44, 44, 49, 49, 48, 84, 83, 119, 118]
  : [31, 31, 31, 33, 33, 33, 32, 32, 32, 36, 36, 36, 34, 34, 34, 35, 35, 35]

const face = () => {
  if (stats.fail > 0) {
    return '( x .x)'
  } else if (stats.todo > 0) {
    return '( > .<)'
  } else if (stats.skip > 0) {
    return '( o .o)'
  } else if (stats.pass > 0) {
    return '( ^ .^)'
  } else {
    return '( - .-)'
  }
}

const frame = () => {
  let result = ''

  if (stats.tick !== 0) {
    result += '\x1b[25l\x1b[4A'
  }

  const pass = localNum.format(stats.pass)
  const fail = localNum.format(stats.fail)
  const todo = localNum.format(stats.todo)
  const skip = localNum.format(stats.skip)
  const sumWidth = Math.max(pass.length, fail.length, skip.length)

  result += ' '.repeat(sumWidth - pass.length) + `\x1b[32m${pass}\x1b[0m\n`
  result += ' '.repeat(sumWidth - fail.length) + `\x1b[31m${fail}\x1b[0m\n`
  result += ' '.repeat(sumWidth - todo.length) + `\x1b[33m${todo}\x1b[0m\n`
  result += ' '.repeat(sumWidth - skip.length) + `\x1b[36m${skip}\x1b[0m\n`
  result += '\x1b[4A'

  if (is256) {
    line.push(`\x1b[38;5;${colors[stats.tick % 39]}m` + ((stats.tick % 2 === 0) ? '_' : '-'))
  } else {
    line.push(`\x1b[${colors[stats.tick % 18]}m` + ((stats.tick % 2 === 0) ? '_' : '-'))
  }
  if (line.length > width) {
    line.shift()
  }

  result += `\x1b[${sumWidth}C ${line.join('')}\x1b[0m\n`
  result += `\x1b[${sumWidth}C ${line.join('')}\x1b[0m\n`
  result += `\x1b[${sumWidth}C ${line.join('')}\x1b[0m\n`
  result += `\x1b[${sumWidth}C ${line.join('')}\x1b[0m\n`
  result += '\x1b[4A'

  const catPad = sumWidth + 1 + line.length

  if (stats.tick % 2 === 0) {
    result += `\x1b[${catPad}C_,------,\n`
    result += `\x1b[${catPad}C_|   /\\_/\\\n`
    result += `\x1b[${catPad}C^|__${face()}\n`
    result += `\x1b[${catPad}C  ""  ""\n`
  } else {
    result += `\x1b[${catPad}C_,------,\n`
    result += `\x1b[${catPad}C_|  /\\_/\\ \n`
    result += `\x1b[${catPad}C~|_${face()} \n`
    result += `\x1b[${catPad}C ""  "" \n`
  }

  stats.tick += 1
  return result
}

const printFailures = () => {
  let result = ''

  const numWidth = localNum.format(stats.failureDetails.length).length
  const indent = ' '.repeat(numWidth + 2)

  for (let i = 0; i < stats.failureDetails.length; i += 1) {
    const n = localNum.format(i + 1)
    const details = stats.failureDetails[i]
    if (details.data.todo) {
      result += `\n\x1b[33m${' '.repeat(numWidth - n.length)}${n}. ${details.nest}\x1b[0m`
      result += (typeof details.data.todo === 'string')
        ? ` # TODO ${details.data.todo}\n`
        : ' # TODO\n'
    } else {
      result += `\n\x1b[31m${' '.repeat(numWidth - n.length)}${n}. ${details.nest}\x1b[0m\n`
    }
    result += `${indent}${details.data.file}:${details.data.line}:${details.data.column}\n`
    const { message, ...cause } = details.data.details.error.cause

    result += `${indent}${message} ${JSON.stringify(cause, null, indent + '  ').replace('\n}', `\n${indent}}`)}\n`
  }

  return result
}

const report = ms => {
  let result = ''
  if (ms > 1000) {
    result += `Finished in ${localNum.format(ms / 1000)} seconds\n`
  } else {
    result += `Finished in ${localNum.format(ms)} milliseconds\n`
  }

  if (stats.fail + stats.todo > 0) {
    const error = (stats.fail > 0) ? '\x1b[31m' : '\x1b[33m'
    const total = stats.fail + stats.todo + stats.skip + stats.pass

    if (total === 1) {
      result += `${error}1 test, 1 failure\x1b[0m\n`
    } else if (stats.fail + stats.todo === 1) {
      if (stats.skip === 0) {
        result += `${error}${total} tests, 1 failure\x1b[0m\n`
      } else {
        result += `${error}${total} tests, 1 failure \x1b[36m(${stats.skip} skipped)\x1b[0m\n`
      }
    } else {
      if (stats.skip === 0) {
        result += `${error}${total} tests, ${stats.fail + stats.todo} failures\x1b[0m\n`
      } else {
        result += `${error}${total} tests, ${stats.fail + stats.todo} failures \x1b[36m(${stats.skip} skipped)\x1b[0m\n`
      }
    }

    result += printFailures()
  } else if (stats.skip > 0) {
    const total = stats.skip + stats.pass
    if (total === 1) {
      result += '\x1b[32m1 test, 0 failures \x1b[36m(1 skipped)\x1b[0m\n'
    } else {
      result += `\x1b[32m${total} tests, 0 failures \x1b[36m(${stats.skip} skipped)\x1b[0m\n`
    }
  } else if (stats.pass > 1) {
    result += `\x1b[32m${stats.pass} tests, 0 failures\x1b[0m\n`
  } else if (stats.pass === 1) {
    result += '\x1b[32m1 test, 0 failures\x1b[0m\n'
  } else {
    result += '\x1b[32mNo tests found\x1b[0m\n'
  }

  return result
}

export default async function * whwhw (source) {
  for await (const { type, data } of source) {
    switch (type) {
      case 'test:pass':
        if (data.skip === undefined) {
          if (data.details.type === undefined) {
            stats.pass += 1

            if (data.todo !== undefined) {
              stats.todoDetails.push({
                nest: stats.nest.join(' ⇒ '),
                detail: data.todo
              })
            }
          }
        } else {
          stats.skip += 1
          stats.todoDetails.push({
            nest: stats.nest.join(' ⇒ '),
            detail: data.skip
          })
        }
        break
      case 'test:fail':
        if (data.details.type === undefined) {
          if (data.todo === undefined) {
            stats.fail += 1
          } else {
            stats.todo += 1
          }
          stats.failureDetails.push({ nest: stats.nest.join(' ⇒ '), data })
        }
        break
      case 'test:start':
        while (stats.nest.length > data.nesting) {
          stats.nest.pop()
        }
        stats.nest.push(data.name)
        yield frame()
        break
      case 'test:diagnostic':
        if (data.message.match(/^duration_ms +([0-9]*[.][0-9]*)$/) !== null) {
          yield frame() + report(JSON.parse(data.message.match(/^duration_ms +([0-9]*[.][0-9]*)$/)[1]))
        }
        break
    }
  }
}
