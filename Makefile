# Weightlifting Warm-Up Set Plate Calculator
# Copyright (C) 2024  Matthew Alexander LaChance
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
.PHONY: test features dev lint clean dist-clean

test: node_modules lint
	node --test --test-reporter=./nyan.js --test-concurrency=8

features:
	bin/container test
	bin/container features

dev:
	bin/container dev

lint: node_modules
	npm exec standard

node_modules: package-lock.json
	npm install

package-lock.json: package.json
	npm install

# Use containers to build the Gemfile.lock. Use the ruby container that
# we're using for feature tests.
features/support/Gemfile.lock: features/support/Gemfile
	podman run --rm -v `pwd`/features/support:/usr/src/app -w /usr/src/app `grep '/ruby:.* AS features$$' Containerfile | sed -e 's/^FROM //' -e 's/ AS features$$//'` bundle update
	podman run --rm -v `pwd`/features/support:/usr/src/app -w /usr/src/app `grep '/ruby:.* AS features$$' Containerfile | sed -e 's/^FROM //' -e 's/ AS features$$//'` bundle install

clean:
	rm -rf node_modules

dist-clean: clean
	rm -f package-lock.json features/support/Gemfile.lock
