# Weightlifting Warm-Up Set Plate Calculator
# Copyright (C) 2024  Matthew Alexander LaChance
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
FROM docker.io/library/node:22.9.0 AS test

WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY ./lib lib/
COPY ./test test/
CMD ["npm", "test"]


FROM docker.io/library/nginx:1.27.2 AS web
ENV NGINX_PORT=80

COPY features/support/nginx.conf /etc/nginx/templates/default.conf.template
COPY lib /usr/share/nginx/html/


FROM docker.io/library/ruby:3.3.5 AS features
ENV APP_URL="http://web:80"
ENV CUCUMBER_PUBLISH_QUIET="true"

RUN useradd -d /tmp tester \
      && bundle config --global frozen 1 \
      && apt-get update \
      && apt-get install -y firefox-esr \
      && wget -O /tmp/driver.tar.gz https://github.com/mozilla/geckodriver/releases/download/v0.35.0/geckodriver-v0.35.0-linux64.tar.gz \
      && tar -xzf /tmp/driver.tar.gz -C /usr/local/bin

WORKDIR /usr/src/app
COPY features/support/Gemfile features/support/Gemfile.lock ./
RUN bundle install

COPY ./features features/

USER tester
ENTRYPOINT ["bundle", "exec"]
CMD ["cucumber", "--color", "--tags", "not @draft"]
