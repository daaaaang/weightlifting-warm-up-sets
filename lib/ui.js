// Weightlifting Warm-Up Set Plate Calculator
// Copyright (C) 2024  Matthew Alexander LaChance
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { Gym } from './gym.js'

const gym = {}

const reweightGym = () => {
  const currentSelected = document.getElementById('goal-weight').value
  document.getElementById('goal-weight').innerHTML = gym.bar.targets().map(x => {
    const value = `${x.numerator}/${x.denominator}`
    if (value === currentSelected) {
      return `<option value="${value}" selected>${x} lbs</option>`
    } else {
      return `<option value="${value}">${x} lbs</option>`
    }
  }).join('')
  document.getElementById('weight-list').innerHTML = gym.setup.plates().map(x => {
    return `<li>${JSON.stringify(x)}</li>`
  }).join('')
  calculate()
}

const emptyGym = () => {
  gym.setup = Gym({
    bars: [{ name: 'the only barbell', weight: document.getElementById('barbell-weight').value, type: 'olympic' }],
    plates: []
  })
  gym.bar = gym.setup.bars()[0]
  reweightGym()
}

const mattGym = () => {
  gym.setup = Gym({
    bars: [{ name: 'the only barbell', weight: document.getElementById('barbell-weight').value, type: 'olympic' }],
    plates: [45, 45, 35, 25, 10, 5, 5, 2.5, 1, 0.75, 0.5, 0.25]
  })
  gym.bar = gym.setup.bars()[0]
  reweightGym()
}

const changeBarbell = () => {
  gym.setup = Gym({
    bars: [{ name: 'the only barbell', weight: document.getElementById('barbell-weight').value, type: 'olympic' }],
    plates: gym.setup.plates()
  })
  gym.bar = gym.setup.bars()[0]
  reweightGym()
}

const addNewPlates = () => {
  gym.setup = Gym({
    bars: gym.setup.bars(),
    plates: gym.setup.plates().concat([document.getElementById('new-plate-weight').value])
  })
  gym.bar = gym.setup.bars()[0]
  reweightGym()
}

const calculate = () => {
  const isCoolDown = document.getElementById('cool-down').checked
  const plan = gym.bar.plan(document.getElementById('goal-weight').value, {
    minSteps: Number(document.getElementById('minimum-sets').value) + 1
  }).map(x => x.weight)
  const withCoolDown = (isCoolDown)
    ? plan.concat([gym.bar.nearest(plan[plan.length - 1].mul(0.75))])
    : plan
  const loads = gym.bar.loadsFor(withCoolDown).map(x => x.map(y => y.weight))

  const planHTML = []
  for (let i = 0; i < plan.length - 1; i += 1) {
    planHTML.push(step(`Warm-up Set ${i + 1} of ${plan.length - 1}`,
                       `${plan[i]} lbs.`,
                       loads[i]))
  }

  planHTML.push(step('Main Set',
                     `${plan[plan.length - 1]} lbs.`,
                     loads[plan.length - 1]))

  if (isCoolDown) {
    planHTML.push(step('Cool-down Set',
                       `${withCoolDown[plan.length]} lbs.`,
                       loads[plan.length]))
  }

  document.getElementById('grid').innerHTML = planHTML.join('')
}

const step = (name, weight, load) => {
  const html = ['<div class="fl w-100 w-50-m w-25-l pv2 ph2-ns">']
  html.push(`<p><strong class="set-heading">${name}: ${weight}</strong><br/><em>`)
  if (load.length === 0) {
    html.push('empty bar')
  } else {
    html.push(load.join(' + '))
  }
  html.push('</em></p></div>')
  return html.join('')
}

const init = () => {
  mattGym()

  document.getElementById('goal-weight').addEventListener('change', calculate)
  document.getElementById('minimum-sets').addEventListener('change', calculate)
  document.getElementById('cool-down').addEventListener('change', calculate)
  document.getElementById('barbell-weight').addEventListener('change', changeBarbell)
  document.getElementById('empty-gym').addEventListener('click', emptyGym)
  document.getElementById('add-new-plate').addEventListener('click', addNewPlates)
  calculate()
}

export { init }
