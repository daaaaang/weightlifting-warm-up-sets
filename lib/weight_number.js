// Weightlifting Warm-Up Set Plate Calculator
// Copyright (C) 2024  Matthew Alexander LaChance
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const gcd = (a, b) => (b === 0) ? a : gcd(b, a % b)

const WeightNumber = (inputN, inputD) => {
  let n
  let d

  if (typeof inputD === 'undefined') {
    if (typeof inputN.numerator !== 'undefined' && typeof inputN.denominator !== 'undefined') {
      n = Number(inputN.numerator)
      d = Number(inputN.denominator)
    } else if (typeof inputN === 'string' && inputN.match(/\//) !== null) {
      const match = inputN.match(/^(.+)\/(.+)$/)
      n = Number(match[1])
      d = Number(match[2])
    } else {
      n = Number(inputN)
      d = 1
    }

    if (isNaN(n) || isNaN(d)) {
      throw new TypeError(`got NaN from WeightNumber(${JSON.stringify(inputN)})`)
    }
  } else {
    n = Number(inputN)
    d = Number(inputD)

    if (isNaN(n) || isNaN(d)) {
      throw new TypeError(`got NaN from WeightNumber(${JSON.stringify(inputN)}, ${JSON.stringify(inputD)})`)
    }
  }

  if (d === 0) {
    throw new TypeError('cannot divide by zero')
  }

  const commonDivisor = gcd(n, d)
  n /= commonDivisor
  d /= commonDivisor

  const numerator = (d < 0) ? -n : n
  const denominator = (d < 0) ? -d : d
  const toNumber = () => numerator / denominator

  return {
    numerator,
    denominator,
    toNumber,
    toString: () => {
      const whole = Math.floor(numerator / denominator)
      const remainder = numerator % denominator
      if (remainder === 0) {
        return whole.toString()
      }

      switch (true) {
        case remainder === 1 && denominator === 4: // 1/4
          return (whole === 0) ? '\u00bc' : `${whole}\u2064\u00bc`
        case remainder === 1 && denominator === 2: // 1/2
          return (whole === 0) ? '\u00bd' : `${whole}\u2064\u00bd`
        case remainder === 3 && denominator === 4: // 3/4
          return (whole === 0) ? '\u00be' : `${whole}\u2064\u00be`
        case remainder === 1 && denominator === 7: // 1/7
          return (whole === 0) ? '\u2150' : `${whole}\u2064\u2150`
        case remainder === 1 && denominator === 9: // 1/9
          return (whole === 0) ? '\u2151' : `${whole}\u2064\u2151`
        case remainder === 1 && denominator === 10: // 1/10
          return (whole === 0) ? '\u2152' : `${whole}\u2064\u2152`
        case remainder === 1 && denominator === 3: // 1/3
          return (whole === 0) ? '\u2153' : `${whole}\u2064\u2153`
        case remainder === 2 && denominator === 3: // 2/3
          return (whole === 0) ? '\u2154' : `${whole}\u2064\u2154`
        case remainder === 1 && denominator === 5: // 1/5
          return (whole === 0) ? '\u2155' : `${whole}\u2064\u2155`
        case remainder === 2 && denominator === 5: // 2/5
          return (whole === 0) ? '\u2156' : `${whole}\u2064\u2156`
        case remainder === 3 && denominator === 5: // 3/5
          return (whole === 0) ? '\u2157' : `${whole}\u2064\u2157`
        case remainder === 4 && denominator === 5: // 4/5
          return (whole === 0) ? '\u2158' : `${whole}\u2064\u2158`
        case remainder === 1 && denominator === 6: // 1/6
          return (whole === 0) ? '\u2159' : `${whole}\u2064\u2159`
        case remainder === 5 && denominator === 6: // 5/6
          return (whole === 0) ? '\u215a' : `${whole}\u2064\u215a`
        case remainder === 1 && denominator === 8: // 1/8
          return (whole === 0) ? '\u215b' : `${whole}\u2064\u215b`
        case remainder === 3 && denominator === 8: // 3/8
          return (whole === 0) ? '\u215c' : `${whole}\u2064\u215c`
        case remainder === 5 && denominator === 8: // 5/8
          return (whole === 0) ? '\u215d' : `${whole}\u2064\u215d`
        case remainder === 7 && denominator === 8: // 7/8
          return (whole === 0) ? '\u215e' : `${whole}\u2064\u215e`
        default:
          return (numerator / denominator).toString()
      }
    },
    double: () => WeightNumber(numerator * 2, denominator),
    half: () => WeightNumber(numerator, denominator * 2),
    add: rhs => {
      const rhsWN = WeightNumber(rhs)
      return WeightNumber(numerator * rhsWN.denominator + denominator * rhsWN.numerator, denominator * rhsWN.denominator)
    },
    sub: rhs => {
      const rhsWN = WeightNumber(rhs)
      return WeightNumber(numerator * rhsWN.denominator - denominator * rhsWN.numerator, denominator * rhsWN.denominator)
    },
    mul: rhs => {
      const rhsWN = WeightNumber(rhs)
      return WeightNumber(numerator * rhsWN.numerator, denominator * rhsWN.denominator)
    },
    cmp: rhs => {
      const rhsWN = WeightNumber(rhs)
      switch (true) {
        case numerator === rhsWN.numerator && denominator === rhsWN.denominator:
          return 0
        case toNumber() < rhsWN.toNumber():
          return -1
        default:
          return 1
      }
    },
    difference: rhs => {
      const rhsWN = WeightNumber(rhs)
      switch (true) {
        case numerator === rhsWN.numerator && denominator === rhsWN.denominator:
          return 0
        case toNumber() < rhsWN.toNumber():
          return WeightNumber(denominator * rhsWN.numerator - numerator * rhsWN.denominator, denominator * rhsWN.denominator)
        default:
          return WeightNumber(numerator * rhsWN.denominator - denominator * rhsWN.numerator, denominator * rhsWN.denominator)
      }
    }
  }
}

export { WeightNumber }
