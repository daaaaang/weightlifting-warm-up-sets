// Weightlifting Warm-Up Set Plate Calculator
// Copyright (C) 2024  Matthew Alexander LaChance
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { WeightNumber } from './weight_number.js'

const Gym = gymInit => {
  const equipment = {}

  const Bar = barInit => {
    const bar = {}
    const barMemo = {}

    // Can init with a number or with any object with a `weight`.
    if (typeof barInit.weight === 'undefined') {
      bar.weight = WeightNumber(barInit)
    } else {
      for (const parameter in barInit) {
        if (parameter === 'weight') {
          bar[parameter] = WeightNumber(barInit[parameter])
        } else {
          bar[parameter] = barInit[parameter]
        }
      }
    }

    bar.targets = () => {
      if (typeof barMemo.targets === 'undefined') {
        // Since this is potentially expensive, we memoize it.
        const targetsForThisBar = equipment.plates.targetsFor(bar.weight)
        if (typeof bar.ifTooHeavy === 'undefined') {
          barMemo.targets = targetsForThisBar
        } else {
          const lowWeightTargets = equipment.plates
            .targetsFor(barsById[bar.ifTooHeavy].weight)
            .filter(n => n.cmp(bar.weight) < 0)
          barMemo.targets = lowWeightTargets.concat(targetsForThisBar)
        }
        barMemo.plan = appropriatePlanFunctionFor(barMemo.targets)
      }

      return barMemo.targets
    }

    bar.loadFor = x => bar.loadsFor([x])[0]

    bar.loadsFor = a => {
      const loads = []
      let selections = []
      for (let i = 0; i < a.length; i += 1) {
        loads.push(equipment.plates.loadsFor(WeightNumber(a[i]).sub(bar.weight).half()))
        selections.push(0)
        if (loads[i].length === 0) {
          throw new Error(`Cannot load weight ${a[i]}`)
        }
      }

      let currentInMotion = compareLoads([], loads[0][0])

      for (let i = 1; i <= a.length; i += 1) {
        const next = compareLoads(loads[i - 1][0].plates, (i === a.length) ? [] : loads[i][0].plates)
        currentInMotion.weight += next.weight
        currentInMotion.plateCount += next.plateCount
      }

      const recurse = (previous, totalSoFar, i) => {
        if (i === a.length) {
          const k = previous[previous.length - 1]
          const next = compareLoads(loads[i - 1][k].plates, [])
          const total = {
            weight: next.weight + totalSoFar.weight,
            plateCount: next.plateCount + totalSoFar.plateCount
          }

          if (total.weight < currentInMotion.weight) {
            selections = previous.concat([k])
            currentInMotion = total
          } else if (total.weight === currentInMotion.weight) {
            if (total.plateCount < currentInMotion.plateCount) {
              selections = previous.concat([k])
              currentInMotion = total
            }
          }
        } else if (i === 0) {
          for (let j = 0; j < loads[i].length; j += 1) {
            recurse([j], compareLoads([], loads[i][j].plates), i + 1)
          }
        } else {
          const k = previous[previous.length - 1]
          for (let j = 0; j < loads[i].length; j += 1) {
            const next = compareLoads(loads[i - 1][k].plates, loads[i][j].plates)
            const total = {
              weight: next.weight + totalSoFar.weight,
              plateCount: next.plateCount + totalSoFar.plateCount
            }
            recurse(previous.concat([j]), total, i + 1)
          }
        }
      }

      recurse([], {}, 0)

      const final = []
      for (let i = 0; i < a.length; i += 1) {
        final.push(loads[i][selections[i]].plates)
      }
      return final
    }

    const compareLoads = (a, b) => {
      let i = 0
      for (i; i < Math.min(a.length, b.length); i += 1) {
        if (a[i].weight.cmp(b[i].weight) !== 0) {
          break
        }
      }

      const totalMoved = {
        weight: 0,
        plateCount: 0
      }

      for (let j = i; j < a.length; j += 1) {
        totalMoved.weight += a[j].weight.toNumber()
        totalMoved.plateCount += 1
      }

      for (let j = i; j < b.length; j += 1) {
        totalMoved.weight += b[j].weight.toNumber()
        totalMoved.plateCount += 1
      }

      return totalMoved
    }

    bar.plan = (goal, options) => {
      const targetWeight = bar.nearest(goal)
      const o = (typeof options === 'undefined') ? {} : options
      return barMemo.plan(targetWeight, {
        maxStep: (typeof o.maxStep === 'undefined') ? 45 : o.maxStep,
        minSteps: (typeof o.minSteps === 'undefined') ? 1 : o.minSteps,
        startWeight: (typeof o.startWeight === 'undefined') ? bar.weight : WeightNumber(o.startWeight)
      }).map(weight => {
        if (weight.cmp(bar.weight) < 0) {
          return { bar: barsById[bar.ifTooHeavy], weight }
        } else {
          return { bar, weight }
        }
      })
    }

    bar.nearest = x => {
      return findWeight(x).nearestDown
    }

    bar.nearestUp = x => {
      return findWeight(x).nearestUp
    }

    bar.roundDown = x => {
      return findWeight(x).roundDown
    }

    bar.roundUp = x => {
      return findWeight(x).roundUp
    }

    const findWeight = x => findPlaceInSortedList(WeightNumber(x), bar.targets(), (a, b) => a.cmp(b), (a, b) => a.difference(b))

    const appropriatePlanFunctionFor = targets => {
      if (targets.length > 1) {
        const step = targets[1].sub(targets[0])
        for (let i = 2; i < targets.length; i += 1) {
          if (targets[i].cmp(targets[i - 1].add(step)) !== 0) {
            return unevenPlan
          }
        }
      }

      return evenlyDistributedPlan
    }

    const evenlyDistributedPlan = (x, o) => {
      const startWeight = o.startWeight
      const stepCount = Math.max(Math.ceil(x.sub(startWeight).toNumber() / o.maxStep), o.minSteps - 1)
      const stepWeight = (stepCount === 0) ? 0 : x.sub(startWeight).toNumber() / stepCount
      const idealSecondTarget = startWeight.toNumber() + stepWeight
      const littleStep = bar.roundDown(idealSecondTarget).sub(startWeight)
      const bigStep = bar.roundUp(idealSecondTarget).sub(startWeight)

      let bigStepCount = 0
      while (startWeight.add(littleStep.mul(stepCount - bigStepCount)).add(bigStep.mul(bigStepCount)).cmp(x) < 0) {
        bigStepCount += 1
      }

      let currentTarget = startWeight
      const plan = [currentTarget]
      for (let i = 0; i < stepCount; i += 1) {
        currentTarget = bar.nearest(currentTarget.add((i < bigStepCount) ? bigStep : littleStep))
        plan.push(currentTarget)
      }

      return plan
    }

    const unevenPlan = (x, o) => {
      const stepCount = Math.max(Math.ceil(x.sub(bar.weight).toNumber() / o.maxStep), o.minSteps - 1)
      const shortest = unevenPlanHelper(x, o, stepCount)

      if (shortest.maxDiff > o.maxStep) {
        const longer = unevenPlanHelper(x, o, stepCount + 1)
        if (longer.maxDiff < shortest.maxDiff) {
          return longer.plan
        }
      }

      return shortest.plan
    }

    const unevenPlanHelper = (x, o, stepCount) => {
      const stepWeight = (stepCount === 0) ? 0 : x.sub(bar.weight).toNumber() / stepCount
      const plan = [bar.weight]
      let maxDiff = 0
      for (let i = 1; i <= stepCount; i += 1) {
        plan.push(bar.nearestUp(bar.weight.add(stepWeight * i)))
        maxDiff = Math.max(maxDiff, plan[i].sub(plan[i - 1]).toNumber())
      }
      return { maxDiff, plan }
    }

    return bar
  }

  const Plate = plateInit => {
    const plate = {}

    if (typeof plateInit.weight === 'undefined') {
      plate.weight = WeightNumber(plateInit)
    } else {
      for (const parameter in plateInit) {
        if (parameter === 'weight') {
          plate[parameter] = WeightNumber(plateInit[parameter])
        } else {
          plate[parameter] = plateInit[parameter]
        }
      }
    }

    return plate
  }

  if (typeof gymInit === 'undefined' || typeof gymInit.bars === 'undefined') {
    equipment.bars = []
  } else {
    equipment.bars = gymInit.bars.map(Bar)
  }

  equipment.plates = PlateSet()
  if (typeof gymInit !== 'undefined' && typeof gymInit.plates !== 'undefined') {
    gymInit.plates.forEach(plate => {
      equipment.plates.add(Plate(plate))
    })
  }

  const barsById = { }
  equipment.bars.forEach(bar => {
    if (typeof bar.id !== 'undefined') {
      barsById[bar.id] = bar
    }
  })

  return {
    bars: () => equipment.bars,
    barById: id => barsById[id],
    plates: () => gymInit.plates,
    targets: () => []
  }
}

const PlateSet = init => {
  const plateSet = {}
  const plates = HashByWeight()
  const plateMemo = {}

  const clearConfigurations = () => {
    delete plateMemo.configurations
  }

  const memoizeConfigurations = () => {
    if (typeof plateMemo.configurations === 'undefined') {
      plateMemo.configurations = HashByWeight()
      const plateWeights = plates.keys()

      const recurse = (base, plateArray, index) => {
        if (index === plateWeights.length) {
          plateMemo.configurations.add({
            weight: base,
            plates: plateArray
          })
        } else {
          const plate = plates.get(plateWeights[index])[0]

          recurse(base, plateArray, index + 1)

          let load = base
          let morePlates = plateArray
          for (let i = 0; i < plate.pairCount; i += 1) {
            load = load.add(plate.weight)
            morePlates = [plate].concat(morePlates)
            recurse(load, morePlates, index + 1)
          }
        }
      }

      recurse(WeightNumber(0), [], 0)
    }
  }

  plateSet.add = plate => {
    clearConfigurations()
    const existing = plates.get(plate.weight)
    if (existing.length === 0) {
      plate.pairCount = 1
      plates.add(plate)
    } else {
      existing[0].pairCount += 1
    }
  }

  plateSet.targetsFor = barWeight => {
    memoizeConfigurations()
    return plateMemo.configurations.keys().map(x => x.double().add(barWeight))
  }

  plateSet.loadsFor = targetWeight => {
    memoizeConfigurations()
    return plateMemo.configurations.get(targetWeight)
  }

  plateSet.toArray = () => plates.toArray()

  return plateSet
}

const HashByWeight = init => {
  const hash = {}
  const numerators = {}

  const lookup = x => {
    const weightNumber = WeightNumber(x)
    const n = weightNumber.numerator
    const d = weightNumber.denominator

    if (typeof numerators[n] === 'undefined') {
      numerators[n] = {}
      numerators[n][d] = []
    } else if (typeof numerators[n][d] === 'undefined') {
      numerators[n][d] = []
    }

    return numerators[n][d]
  }

  hash.add = thing => {
    lookup(thing.weight).push(thing)
  }

  hash.get = weightNumber => {
    return Array.from(lookup(weightNumber))
  }

  hash.keys = () => {
    const keys = []
    for (const n in numerators) {
      for (const d in numerators[n]) {
        if (numerators[n][d].length > 0) {
          keys.push(WeightNumber(n, d))
        }
      }
    }

    return keys.sort((a, b) => a.cmp(b))
  }

  hash.toArray = () => {
    hash.keys().reduce((acc, cur) => acc.concat(hash.get(cur)), [])
  }

  if (typeof init !== 'undefined') {
    if (Array.isArray(init)) {
      init.forEach(thing => {
        hash.add(thing)
      })
    } else {
      hash.add(init)
    }
  }

  return hash
}

const findPlaceInSortedList = (query, sortedArray, compare, diff) => {
  if (sortedArray.length === 0) {
    return { type: 'emptyArray' }
  }

  const recurse = (i, j) => {
    if (i === j) {
      const c = compare(query, sortedArray[i])

      if (c === 0 || i === 0 || i === sortedArray.length - 1) {
        return {
          type: 'normal',
          roundDown: sortedArray[i],
          roundUp: sortedArray[i],
          nearestDown: sortedArray[i],
          nearestUp: sortedArray[i]
        }
      } else if (c < 0) {
        const less = sortedArray[i - 1]
        const greater = sortedArray[i]
        return {
          type: 'normal',
          roundDown: less,
          roundUp: greater,
          nearestDown: (compare(diff(query, less), diff(query, greater)) > 0) ? greater : less,
          nearestUp: (compare(diff(query, less), diff(query, greater)) < 0) ? less : greater
        }
      } else {
        const less = sortedArray[i]
        const greater = sortedArray[i + 1]
        return {
          type: 'normal',
          roundDown: less,
          roundUp: greater,
          nearestDown: (compare(diff(query, less), diff(query, greater)) > 0) ? greater : less,
          nearestUp: (compare(diff(query, less), diff(query, greater)) < 0) ? less : greater
        }
      }
    } else {
      const k = Math.floor((i + j) / 2)
      const c = compare(query, sortedArray[k])

      if (c === 0) {
        return recurse(k, k)
      } else if (c < 0) {
        return recurse(i, k)
      } else {
        return recurse(k + 1, j)
      }
    }
  }

  return recurse(0, sortedArray.length - 1)
}

export { Gym }
