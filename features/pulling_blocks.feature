# Weightlifting Warm-Up Set Plate Calculator
# Copyright (C) 2024  Matthew Alexander LaChance
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
Feature: In order to perform deadlifts and other from-floor lifts
  As someone with pulling blocks I might rather not use
  I need to be able to add pulling blocks as an optional kind of plate

  Background:
    Given I have a 45lb barbell, 12.5lb 14" pulling blocks, 45lb 13" pulling blocks, and 45lb, 25lb, 10lb, 10lb, 5lb, and 2.5lb plates
    And units are set to pounds
    And I set a maximum increase of 45 lbs between sets
    And cool-down sets are disabled
    And minimum warm-up sets are set to 0

  Scenario: Max weight without pulling blocks
    Given minimum pulling-block sets are set to 0
    When I select a weight of 240 lbs
    Then I should see one main set at 240 lbs with 45lb, 25lb, 10lb, 10lb, 5lb, and 2.5lb plates

  Scenario: Lowest weight that requires using pulling blocks
    Given minimum pulling-block sets are set to 0
    When I select a weight of 245 lbs
    Then I should see four warm-up sets
    And the fourth warm-up set is 205 lbs with 45lb 13" pulling blocks and 25lb plates and 10lb plates
    And I should see one main set at 245 lbs with 45lb 13" pulling blocks and 45lb plates and 10lb plates

  Scenario: Tallest pulling blocks go closest in
    Given minimum pulling-block sets are set to 0
    When I select a weight of 335 lbs
    Then I should see seven warm-up sets
    And the second warm-up set is 90 lbs with 12.5lb 14" pulling blocks and 10lb plates
    And the third warm-up set is 135 lbs with 12.5lb 14" pulling blocks and 25lb, 5lb, and 2.5lb plates
    And the fourth warm-up set is 175 lbs with 12.5lb 14" and 45lb 13" pulling blocks and 5lb plates and 2.5lb plates
    And I should see one main set at 335 lbs with 12.5lb 14" and 45lb 13" pulling blocks and 45lb, 25lb, 10lb, 5lb, and 2.5lb plates

  Scenario: Use heaviest pulling blocks possible
    Given minimum pulling-block sets are set to 1
    When I select a weight of 135 lbs
    Then I should see two warm-up sets
    And the first warm-up set is 45 lbs with an empty bar
    And the second warm-up set is 90 lbs with 10lb, 10lb, and 2.5lb plates
    And I should see one main set at 135 lbs with 45lb 13" pulling blocks

  Scenario: If using lighter pulling blocks, stick with them
    Given minimum pulling-block sets are set to 2
    When I select a weight of 135 lbs
    Then I should see two warm-up sets
    And the first warm-up set is 45 lbs with an empty bar
    And the second warm-up set is 90 lbs with 12.5lb 14" pulling blocks and 10lb plates
    And I should see one main set at 135 lbs with 12.5lb 14" pulling blocks and 25lb, 5lb, and 2.5lb plates
