# Weightlifting Warm-Up Set Plate Calculator
# Copyright (C) 2024  Matthew Alexander LaChance
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
Given("I have {barbell} with {plates}") do |barbell, plates|
  set_up_new_gym(barbell, plates)
end

Given("I have {barbell}, {pulling_blocks}, and {plates}") do |barbell, pulling_blocks, plates|
  set_up_new_gym(barbell, plates, pulling_blocks: pulling_blocks)
end

Given("I set a maximum increase of {weight} between sets") do |weight|
  set_max_warmup_increment(weight)
end

Given("cool-down sets are disabled") do
  disable_cooldown_set
end

Then("the lowest available weight is {weight}") do |weight|
  expect(first_available_goal_weight).to eq weight
end

Then("the highest available weight is {weight}") do |weight|
  expect(last_available_goal_weight).to eq weight
end

Then("each option is separated by {weight} increments") do |weight|
  available_goal_weights.each_cons(2) do |lighter, heavier|
    expect(heavier - lighter).to eq weight
  end
end

When("I select a weight of {weight}") do |weight|
  select_goal_weight(weight)
end

Then("I should see {spelled_out_int} warm-up set at {weight} with {plates}") do |count, weight, plates|
  expect(warmup_set_count).to eq count
  expect(all_warmup_sets[0].heading).to match(/ #{weight}/i)
  expect(all_warmup_sets[0].plates).to eq plates
end

Then("I should see {spelled_out_int} main set at {weight} with {plates}") do |count, weight, plates|
  expect(main_set_count).to eq count
  expect(all_main_sets[0].heading).to match(/ #{weight}/i)
  expect(all_main_sets[0].pulling_blocks).to eq []
  expect(all_main_sets[0].plates).to eq plates
end

Then("I should see {spelled_out_int} main set at {weight} with {pulling_blocks}") do |count, weight, pulling_blocks|
  expect(main_set_count).to eq count
  expect(all_main_sets[0].heading).to match(/ #{weight}/i)
  expect(all_main_sets[0].pulling_blocks).to eq pulling_blocks
  expect(all_main_sets[0].plates).to eq []
end

Then("I should see {spelled_out_int} main set at {weight} with {pulling_blocks} and {plates}") do |count, weight, pulling_blocks, plates|
  expect(main_set_count).to eq count
  expect(all_main_sets[0].heading).to match(/ #{weight}/i)
  expect(all_main_sets[0].pulling_blocks).to eq pulling_blocks
  expect(all_main_sets[0].plates).to eq plates
end

Then("I should see {spelled_out_int} cool-down set at {weight} with {plates}") do |count, weight, plates|
  expect(cooldown_set_count).to eq count
  expect(all_cooldown_sets[0].heading).to match(/ #{weight}/i)
  expect(all_cooldown_sets[0].plates).to eq plates
end

Then("I should see {spelled_out_int} warm-up sets") do |count|
  expect(warmup_set_count).to eq count
end

Then("the {array_index_word} warm-up set is {weight} with {plates}") do |i, weight, plates|
  expect(all_warmup_sets[i].heading).to match(/ #{weight}/i)
  expect(all_warmup_sets[i].pulling_blocks).to eq []
  expect(all_warmup_sets[i].plates).to eq plates
end

Then("the {array_index_word} warm-up set is {weight} with {pulling_blocks}") do |i, weight, pulling_blocks|
  expect(all_warmup_sets[i].heading).to match(/ #{weight}/i)
  expect(all_warmup_sets[i].pulling_blocks).to eq pulling_blocks
  expect(all_warmup_sets[i].plates).to eq []
end

Then("the {array_index_word} warm-up set is {weight} with {pulling_blocks} and {plates}") do |i, weight, pulling_blocks, plates|
  expect(all_warmup_sets[i].heading).to match(/ #{weight}/i)
  expect(all_warmup_sets[i].pulling_blocks).to eq pulling_blocks
  expect(all_warmup_sets[i].plates).to eq plates
end

Given("minimum warm-up sets are set to {int}") do |n|
  set_minimum_warmup_set(n)
end

Given("a cool-down set to {int}%") do |n|
  add_cooldown_set(Rational(n, 100))
end

Given("units are set to {unit_name}") do |unit|
  case unit
  when :kg
    set_weight_unit_to_kg
  when :lb
    set_weight_unit_to_lbs
  end
end

Given("minimum pulling-block sets are set to {int}") do |set_count|
  set_min_pulling_block_sets(set_count)
end
