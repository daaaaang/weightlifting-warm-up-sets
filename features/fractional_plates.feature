# Weightlifting Warm-Up Set Plate Calculator
# Copyright (C) 2024  Matthew Alexander LaChance
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
@draft
Feature: Kilograms and pounds

  Scenario: Fractions as small as 1/8
    Given I have a 20kg barbell with 2.5kg, 1.5kg, 1kg, 0.5kg, 0.25kg, and 0.125kg plates
    And units are set to kilograms
    When I select a weight of 29.75 kg
    Then I should see one main set at 29.75 kg with 2.5kg, 1.5kg, 0.5kg, 0.25kg, and 0.125kg plates

  Scenario: Equipment in both kg and lbs
    Given I have a 20kg barbell with 5lb, 2.5lb, 1lb, 0.75lb, 0.5lb, and 0.25lb plates
    And units are set to pounds
    When I select a weight of 50 lbs
    Then I should see one main set at 50 lbs with 2.5lb plates and 0.5lb plates

  Scenario: Rounding when the barbell is a weird weight
    Given I have a 60.4lb barbell with 10lb plates and 5lb plates
    And units are set to pounds
    Then the lowest available weight is 60 lbs

  Scenario: Rounding when the barbell is a weird weight with fractional plates
    Given I have a 60.4lb barbell with 1lb, 0.75lb, 0.5lb, and 0.25lb plates
    And units are set to pounds
    Then the lowest available weight is 60.5 lbs
