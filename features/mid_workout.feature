# Weightlifting Warm-Up Set Plate Calculator
# Copyright (C) 2024  Matthew Alexander LaChance
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
Feature: In order to facilitate weight training
  As someone who has already set up a gym
  I need to modify the goal weight and minimum warm-up sets easily and often

  Background:
    Given I have a 45lb barbell with 15lb, 10lb, 5lb, and 2.5lb plates
    And units are set to pounds
    And I set a maximum increase of 45 lbs between sets
    And cool-down sets are disabled

  Scenario: Available goal weights are based on plates
    Then the lowest available weight is 45 lbs
    And each option is separated by 5lb increments
    And the highest available weight is 110 lbs

  Scenario: One warm-up set
    Given minimum warm-up sets are set to 0
    When I select a weight of 65 lbs
    Then I should see one warm-up set at 45 lbs with an empty bar
    And I should see one main set at 65 lbs with 10lb plates

  Scenario: One warm-up set with maximum increase
    Given minimum warm-up sets are set to 0
    When I select a weight of 90 lbs
    Then I should see one warm-up set at 45 lbs with an empty bar
    And I should see one main set at 90 lbs with 15lb, 5lb, and 2.5lb plates

  Scenario: Two warm-up sets when exceeding maximum increase
    Given minimum warm-up sets are set to 0
    When I select a weight of 95 lbs
    Then I should see two warm-up sets
    And the first warm-up set is 45 lbs with an empty bar
    And the other warm-up set is 70 lbs with 10lb plates and 2.5lb plates
    And I should see one main set at 95 lbs with 15lb plates and 10lb plates

  Scenario: Minimum warm-up sets
    Given minimum warm-up sets are set to 4
    When I select a weight of 65 lbs
    Then I should see four warm-up sets
    And the first warm-up set is 45 lbs with an empty bar
    And the second warm-up set is 50 lbs with 2.5lb plates
    And the third warm-up set is 55 lbs with 5lb plates
    And the fourth warm-up set is 60 lbs with 5lb plates and 2.5lb plates
    And I should see one main set at 65 lbs with 10lb plates

  Scenario: Use fewest plates possible
    When I select a weight of 75 lbs
    Then I should see one main set at 75 lbs with 15lb plates

  Scenario: Cool down with heavy plates from main set if possible
    Given a cool-down set to 75%
    When I select a weight of 100 lbs
    Then I should see one main set at 100 lbs with 15lb, 10lb, and 2.5lb plates
    And I should see one cool-down set at 75 lbs with 15lb plates
