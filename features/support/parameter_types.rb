# Weightlifting Warm-Up Set Plate Calculator
# Copyright (C) 2024  Matthew Alexander LaChance
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
ParameterType(
  name: "spelled_out_int",
  regexp: /[Oo]ne|[Tt]wo|[Tt]hree|[Ff]our|[Ff]ive|[Ss]ix|[Ss]even|[Ee]ight/,
  type: Integer,
  transformer: lambda do |s|
    case s.downcase
    when "one"
      1
    when "two"
      2
    when "three"
      3
    when "four"
      4
    when "five"
      5
    when "six"
      6
    when "seven"
      7
    when "eight"
      8
    end
  end
)

ParameterType(
  name: "array_index_word",
  regexp: /[Ff]irst|[Ss]econd|[Oo]ther|[Tt]hird|[Ff]ourth/,
  type: Integer,
  transformer: lambda do |s|
    case s.downcase
    when "first"
      0
    when "other"
      1
    when "second"
      1
    when "third"
      2
    when "fourth"
      3
    end
  end
)

ParameterType(
  name: "plates",
  # 123lb, 456lb, [...], and 789lb plates
  # OR
  # 123lb plates and 456lb plates
  # OR
  # 123lb plates
  # OR
  # an empty bar
  regexp: /(?:[0-9.]+(?:lb|kg), )+and [0-9.]+(?:lb|kg) plates|[0-9.]+(?:lb|kg) plates and [0-9.]+(?:lb|kg) plates|[0-9.]+(?:lb|kg) plates|an empty bar/,
  type: Array,
  transformer: lambda do |s|
    s.scan(/([0-9.]+)(lb|kg)/).map { |c| PairOfPlates.new(c[0], c[1].to_sym) }
  end
)

ParameterType(
  name: "barbell",
  regexp: /an? [0-9.]+(?:lb|kg) barbell/,
  type: Barbell,
  transformer: lambda do |s|
    m = /([0-9.]+) ?(lb|kg)/.match(s)
    Barbell.new(m[1], m[2].to_sym)
  end
)

ParameterType(
  name: "weight",
  regexp: /[0-9.]+ ?(?:lbs?|kg)/,
  type: BasicWeight,
  transformer: lambda do |s|
    m = /([0-9.]+) ?(lb|kg)/.match(s)
    BasicWeight.new(m[1], m[2].to_sym)
  end
)

ParameterType(
  name: "unit_name",
  regexp: /pounds|kilograms/,
  type: Symbol,
  transformer: lambda do |s|
    case s
    when "kilograms"
      :kg
    when "pounds"
      :lb
    end
  end
)

ParameterType(
  name: "pulling_blocks",
  regexp: /(?:[0-9.]+(?:lb|kg) [0-9.]+" pulling blocks,? )*[0-9.]+(?:lb|kg) [0-9.]+" pulling blocks|[0-9.]+(?:lb|kg) [0-9.]+" and [0-9.]+(?:lb|kg) [0-9.]+" pulling blocks/,
  type: Array,
  transformer: lambda do |s|
    s.scan(/([0-9.]+)(lb|kg) ([0-9.]+)"/).map { |c| PullingBlocks.new(c[0], c[1].to_sym, c[2]) }
  end
)
