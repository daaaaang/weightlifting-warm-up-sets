# Weightlifting Warm-Up Set Plate Calculator
# Copyright (C) 2024  Matthew Alexander LaChance
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
module DrivesWebBrowser
  def set_up_new_gym(barbell, plates, pulling_blocks: [])
    visit("/")
    empty_gym
    set_barbell_weight(barbell)
    plates.each do |plate|
      fill_in("Add plates", with: plate.decimal)
      click_button("Add Plate Pair")
    end
    pulling_blocks.each do |block|
      fill_in("Pulling block weight", with: block.weight)
      fill_in("Inches from floor", with: block.height)
      click_button("Add Pulling Blocks")
    end
  end

  def empty_gym
    click_button("Empty Gym")
  end

  def set_weight_unit_to_lbs
    choose("Pounds")
  end

  def set_weight_unit_to_kg
    choose("Kilograms")
  end

  def set_barbell_weight(weight)
    fill_in("Barbell", with: weight.decimal)
  end

  def set_max_warmup_increment(_weight)
  end

  def set_min_pulling_block_sets(set_count)
    fill_in("Minimum Pulling Block Sets", with: set_count)
  end

  def add_cooldown_set(_fraction)
    check("cool-down")
  end

  def disable_cooldown_set
    uncheck("cool-down")
  end

  def find_goal_weight_field
    find_field("Goal Weight")
  end

  def available_goal_weights
    find_goal_weight_field.all("option").map do |weight|
      BasicWeight.new(weight.text.sub(/^([0-9.]+) lbs$/, "\\1"), :lb)
    end
  end

  def first_available_goal_weight
    available_goal_weights[0]
  end

  def last_available_goal_weight
    available_goal_weights[-1]
  end

  def select_goal_weight(weight)
    find_goal_weight_field.select(weight.to_s)
  end

  def warmup_set_count
    all(".set-heading", text: /warm-up set/i).size
  end

  def main_set_count
    all(".set-heading", text: /main set/i).size
  end

  def cooldown_set_count
    all(".set-heading", text: /cool-down set/i).size
  end

  def all_warmup_sets
    all(".set-heading", text: /warm-up set/i).map { |s| WeightSet.new(s) }
  end

  def all_main_sets
    all(".set-heading", text: /main set/i).map { |s| WeightSet.new(s) }
  end

  def all_cooldown_sets
    all(".set-heading", text: /cool-down set/i).map { |s| WeightSet.new(s) }
  end

  def set_minimum_warmup_set(n)
    fill_in("Minimum Warm-up Sets", with: n.to_s)
  end
end

class WeightSet
  def initialize(set_heading)
    @set_description = set_heading.ancestor("p")
  end

  def heading
    @heading ||= @set_description.text[/^.+: .+/]
  end

  def plates
    @plates ||= if plate_text == "empty bar"
      []
    else
      plate_text.split(" + ").map do |plate|
        PairOfPlates.new(plate, :lb)
      end
    end
  end

  def pulling_blocks
    []
  end

  def plate_text
    @plate_text ||= @set_description.text[/^(empty bar|[0-9]+(\u2064.)?(?: \+ [0-9]+(\u2064.)?)*)$/]
  end
end

class BasicWeight
  attr_reader :weight, :unit

  def initialize(weight, unit)
    @weight = if weight.instance_of? Rational
      weight
    else
      @weight = Rational(FRACTIONS.reduce(weight) do |memo, fraction|
        memo.sub("\u2064#{fraction[:vulgar]}", fraction[:decimal])
      end)
    end

    @unit = unit
  end

  def inspect
    "<#{self.class.name} #{weight} #{unit}>"
  end

  def to_s
    "#{weight_s} #{unit_s}"
  end

  def decimal
    if weight.denominator == 1
      weight.to_i.to_s
    else
      weight.to_f.to_s
    end
  end

  def ==(other)
    weight == other.weight && unit == other.unit
  end

  def -(other)
    self.class.new(weight - other.weight, unit)
  end

  def weight_s
    if weight.denominator == 1
      weight.to_i.to_s
    else
      FRACTIONS.reduce("%.3f" % weight) do |memo, fraction|
        memo.sub(fraction[:decimal], "\u2064#{fraction[:vulgar]}")
      end
    end
  end

  def unit_s
    case unit
    when :kg
      "kg"
    when :lb
      (weight == 1) ? "lb" : "lbs"
    else
      unit.to_s
    end
  end

  FRACTIONS = [
    {vulgar: "⅛", decimal: ".125"},
    {vulgar: "¼", decimal: ".250"},
    {vulgar: "⅜", decimal: ".375"},
    {vulgar: "½", decimal: ".500"},
    {vulgar: "⅝", decimal: ".625"},
    {vulgar: "¾", decimal: ".750"},
    {vulgar: "⅞", decimal: ".875"}
  ]
end

class PairOfPlates < BasicWeight
end

class Barbell < BasicWeight
end

class PullingBlocks < PairOfPlates
  attr_reader :height

  def initialize(weight, unit, height)
    super(weight, unit)
    @height = Rational(height)
  end
end

World(DrivesWebBrowser)
